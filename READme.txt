# 2D MRS toolbox for processing raw clinical siemens data 
This toolbox is derived from the open source 1D MRS (magnetic resonance spectroscopy) FID-A processing toolbox. 
Please see the 2D_MRS_FIDA_piplineDocument.pdf document for a detailed description of this 2D MRS toolbox. 
