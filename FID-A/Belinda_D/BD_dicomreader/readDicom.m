function output = readDicom(FILENAME)

image = myDicomRead(FILENAME);
info = dicominfo(FILENAME);

info.image = image;

output = info;

end
