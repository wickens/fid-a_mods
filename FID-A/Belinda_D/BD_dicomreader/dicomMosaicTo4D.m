function [image_4D, image_mosaic] = dicomMosaicTo4D(inputDicom)
% Not sure about the dimensions for resolutions and parameters for nSlices
% and nScans... 
% output dimension: imagex, imagey, nSlices, nScans

if length(inputDicom) > 1
    error('Length of dicom is bigger than 1, use dicom{1} as inputDicom instead')
end
nScans = length(inputDicom.instance);
nSlices = double(inputDicom.instance{1}{1}.Private_0019_100a);

tmp = inputDicom.instance{1}{1}.Private_0051_100b;
tmp1 = regexp(tmp, '\D*(?>\d+)*(\d+)', 'tokens');
resolution(1) = str2double(tmp1{1});
resolution(2) = str2double(tmp1{2});

mosaicSize = size(inputDicom.instance{1}{1}.image);
image_4D = zeros(resolution(1), resolution(2), nSlices, nScans);
image_mosaic = zeros(mosaicSize(1), mosaicSize(2), nScans);

nImagesX =  mosaicSize(2) / resolution(2);
nImagesY = mosaicSize(1) / resolution(1);


for scanDx = 1 : nScans
   image_mosaic(:, :, scanDx) =  inputDicom.instance{scanDx}{1}.image;
   
   for sliceDx = 1 : nSlices
       xImDx = mod(sliceDx, nImagesX); 
       if xImDx == 0
           xImDx = nImagesX;
       end
       
       yImDx = ceil(sliceDx / nImagesX);
       
       xStartDx = (xImDx - 1) * resolution(2) + 1;
       yStartDx = (yImDx - 1) * resolution(1) + 1;
       
       image_4D(:, :, sliceDx, scanDx) = image_mosaic(yStartDx : (yStartDx + resolution(1) - 1), xStartDx : (xStartDx + resolution(2) - 1), scanDx);
       
   end
end
