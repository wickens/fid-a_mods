function [dicom, seriesNameArray] = sortDicomIntoFolders(FolderNameOriginal, FolderNameSorted)

if nargin < 2
    [pathstr, name, ext] = fileparts(FolderNameOriginal);
    FolderName = fullfile(pathstr, strcat(name, "_sorted"));
else
    FolderName = FolderNameSorted;
end

[dicom, seriesNameArray] = readDicomFolder(FolderNameOriginal);

if ~exist(FolderName)
    mkdir(FolderName);
end


%%
for scanDx = 1 : length(seriesNameArray)
    
    % makes folder
    scanName = strcat(string(dicom{scanDx}.instance{1}{1}.SeriesNumber), "_", seriesNameArray(scanDx));
    MyFolderInfo = dir(FolderName);
    MyFolderInfo = MyFolderInfo(cellfun(@(x) x, {MyFolderInfo.isdir}));
    MyFolderInfo = MyFolderInfo( cell2mat(cellfun(@(x) ~isempty(regexp(x, '\w')), {MyFolderInfo.name}, 'UniformOutput', false)));
    
    if isempty(find(cell2mat(cellfun(@(x) ~isempty(regexp(x, scanName)), {MyFolderInfo.name}, 'UniformOutput', false))))
        mkdir(fullfile(FolderName, scanName));
    else
        repDx = 1;
        scanName_1 = strcat(scanName, "_", string(repDx));
        
        while(~isempty(find(cell2mat(cellfun(@(x) ~isempty(regexp(x, scanName_1)), {MyFolderInfo.name}, 'UniformOutput', false)))))
            repDx = repDx + 1;
            scanName_1 = strcat(scanName, "_", string(repDx));
        end
        mkdir(fullfile(FolderName, scanName_1));
        scanName = scanName_1;
        
    end
    
    for imagesDx  = 1 : length(dicom{scanDx}.instance)
        copyfile(dicom{scanDx}.instance{imagesDx}{1}.Filename, fullfile(FolderName, scanName));
    end
    
    
end
end