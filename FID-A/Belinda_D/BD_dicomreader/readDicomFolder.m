function [dicom, seriesName] = readDicomFolder(FolderName)

fileList = dir(FolderName);
f = waitbar(0,'Staring...');

newDx = 1;

for fileDx = 1 : length(fileList)
    if fileList(fileDx).isdir ~= 1 && ~isempty(regexp(fileList(fileDx).name, '.IMA$'))
        info = dicominfo(strcat(fileList(fileDx).folder, '\', fileList(fileDx).name));
        info.image = dicomread(strcat(fileList(fileDx).folder, '\', fileList(fileDx).name));
        dicomTemp{newDx} = info;
        
        SeriesInstanceUID{newDx} = info.SeriesInstanceUID;
        SeriesNumber(newDx) = info.SeriesNumber;
        
        newDx = newDx + 1;
    end
    
    waitbar(fileDx/(length(fileList) - 2)  * 0.9, f, sprintf(string('Loading dicoms: %d / %d'), fileDx, (length(fileList) - 2)));
    
end

% sorting it

G = findgroups(SeriesInstanceUID);

nGroups = max(G);

for GDx = 1 : nGroups
    clear groupList
    groupList = find(G == GDx);
    
    %    imageSize = size(dicomTemp{groupList(1)}.image);
    %    imageMatrix = zeros(numel(groupList), size(1), size(2));
    dicom{GDx, 1}.Description{1} = cellstr(dicomTemp{groupList(1)}.SeriesDescription);
    
    seriesName(GDx, 1) = string(dicomTemp{groupList(1)}.SeriesDescription);
    
    for Dx = 1 : numel(groupList)
        dicom{GDx}.instance{Dx} = dicomTemp(groupList(Dx));
   end
    
    waitbar(0.90 + GDx/nGroups * 0.1, f, sprintf(string('Sorting dicoms: %d / %d'), GDx, nGroups));
end

waitbar(1, f, string('Done'));
pause(.5)
delete(f);
end
