function [] = plotAllDicom(filepath)
myFolderInfo = dir(filepath);

for iDx = 1: length(myFolderInfo)
     tmp = regexp(myFolderInfo(iDx).name, '(.*).IMA', 'tokens');
     
     if ~isempty(tmp)
         im = double(dicomread(fullfile(myFolderInfo(iDx).folder, myFolderInfo(iDx).name)));        
         a = dicominfo(fullfile(myFolderInfo(iDx).folder, myFolderInfo(iDx).name));
         scanName = strcat(string(a.SeriesNumber), '_', a.ProtocolName, '_', string(a.InstanceNumber));
         
         f = figure(1); clf; 
         imagesc(im); colormap gray; title(strrep(scanName, '_', ' ')); colorbar;
         xticks([]); yticks([]);
%          saveas(f, strcat(scanName, "_scaled"), 'png');
        saveas(f, fullfile(filepath, char(scanName)), 'png');
     end
end