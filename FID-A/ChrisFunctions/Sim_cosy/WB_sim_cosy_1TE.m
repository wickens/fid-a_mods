% Here I want to simulate 1 TE increment of cosy using my function
% sim_cosy_1TE.m - i.e no localisation 

n= 2048;
sw=5000; %  [Hz]
Bfield=2.89; %[T]
linewidth=10; %[Hz]
tau1=30; % [s] of first press Spin Echo
tau2=20; % 
spinSys='H2O';
load spinSystems
sys=eval(['sys' spinSys]);
inc=0.4; % L cosy increment [ms] 

% simulate 2D COSY
cosy = cell(64,1); 

for i=1:64 % 64 TE measurements  
    TEpntr=i-1;  % TE pointer or tracker
    tau2_new = tau2+(TEpntr*inc); % New TE
    
    out = sim_cosy_1TE(n,sw,Bfield,linewidth,sys,tau1,tau2_new);
    
    cosy{i,1}=out;
end

%% Extract 2D cosy to form a 2D matrix 

[FIDA_data_t2t1,FIDA_data_f2t1,FIDA_data_f2f1,ppm2,freq1] = extract_JP_FIDA_cw(cosy,64,inc,sw); 





