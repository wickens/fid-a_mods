%%Chris w: It works now after turning restaring my computer
% Chris w: this code has been modified so it can readin 2D JPRESS data
% and processes each individual TE sepreately 

%run_pressproc.m
% Jamie Near, McGill University 2014.
% 
% USAGE:
% [out,out_w,out_noproc,out_w_noproc]=run_pressproc_cw(filestring,aaDomain,tmaxin,iterin);
% 
% DESCRIPTION:
% Processing script for Siemens PRESS MRS data in .dat format (twix raw data).  
% Includes combination of reciever channels, removal of bad averages, 
% freqeuncy drift correction, and leftshifting.
% 
% INPUTS:
% filestring    = String variable for the name of the directory containing
%                   the water suppressed .dat file.  Water unsuppressed
%                   .dat file should be contained in [filestring '_w/'];
% aaDomain      = (Optional) Perform the spectral registration (drift correction) using
%                   the full spectrum ('t'), or only a limited frequency
%                   range ('f').  Default is 'f'.
% tmaxin        = (Optional).  Duration (in sec.) of the time domain signal
%                   used in the spectral registration (drift correction).
%                   Default is 0.2 sec.
% iterin        = (Optional).  Maximum number of allowed iterations for the spectral
%                   registration to converge. Default is 20.
% 
% OUTPUTS:
% out           = Fully processed, water suppressed output spectrum.
% out_w         = Fully processed, water unsuppressed output spectrum.
% out_noproc    = Water suppressed output spectrum without pre-
%                   processing (No bad-averages removal, no frequency drift
%                   correction).
% out_w_noproc  = Water unsuppressed output spectrum without pre-
%                   processing.

function [relivant_data]=run_pressproc_cw(filestring,aaDomain,tmaxin,iterin);

if nargin<4
    iterin=20;
     if nargin<3
        tmaxin=0.2;
         if nargin<2
            aaDomain='f';
         end
     end
 end


%Find the filename of the first MEGA_GABA dataset
close all
unixString=['dir ' filestring '\*.dat /s/b'];  % chris w: modified so this works on my windows pc 

[status, filename1]=unix(unixString);
filename1=filename1(1:end-1);

%filename1 = 'C:\Users\My PC\OneDrive\Cambridge\JPRESS\JP_Prisma\TWIX\Invivo\meas_MID00546_FID00678_svs_st_ws_ON_1.dat';

 unixStringw=['dir ' filestring '_w\*.dat /s/b'];
 [status,filename2]=unix(unixStringw);
 filename2=filename2(1:end-1);
%filename2 = 'C:\Users\My PC\OneDrive\Cambridge\JPRESS\JP_Prisma\TWIX\Invivo\meas_MID00572_FID00704_svs_st_ws_OFF_1.dat';
%read in the data.
out_raw=io_loadJPRESS_twix_CW(filename1);

Num_Measurements=size(out_raw);  %chris w
Num_Measurements=Num_Measurements(1:end-1);         %chris w: record number of measurents for loop counter - assume water supressed and water unsupressed have same number of measurements 


relivant_data=cell(Num_Measurements,1);            %chris w: cell to contain all relevant data (processed and unprocessed)from all measurements.
for i=1:Num_Measurements   % chris w: loop through all the measurements 

%load water unsuppressed data and find the coil phases:
if exist(filename2)
    if i==1                                   % chris w: only load TWIX data set when i=1 (i.e. for first itteration)  
        out_w_raw=io_loadJPRESS_twix_CW(filename2);
    end
    
    coilcombos=op_getcoilcombos(out_w_raw{i,1},out_w_raw{i,1}.pointsToLeftshift+1);      % chris W: look for a specific stucture in the cell array (i.e. out_raw_w{i,1};)
else
    coilcombos=op_getcoilcombos(op_averaging(out_raw{i,1}),out_raw{i,1}.pointsToLeftshift+1);      % chris W: look for a specific stucture in the cell array (i.e. out_raw{i,1};)
end
 

%now combine the coil channels:
[out_cc,fid_pre,spec_pre,ph,sig]=op_addrcvrs(out_raw{i,1},out_raw{i,1}.pointsToLeftshift+1,'w',coilcombos);  % chris W: look for a specific stucture in the cell array (i.e. out_raw{i,1};) 
if exist(filename2)
    [out_w_cc,fid_w_pre,spec_w_pre,ph_w,sig_w]=op_addrcvrs(out_w_raw{i,1},out_w_raw{i,1}.pointsToLeftshift+1,'w',coilcombos);  % chris W: look for a specific stucture in the cell array (i.e. out_raw_w{i,1};)
end
%make the un-processed spectra:
out_noproc=op_leftshift(op_averaging(out_cc),out_cc.pointsToLeftshift);
if exist(filename2)
    out_w_noproc=op_leftshift(op_averaging(out_w_cc),out_w_cc.pointsToLeftshift);
end

%plot the data before and after coil phasing:
figure('position',[0 0 560 420]);
subplot(2,1,1);
plot(out_raw{i,1}.ppm,out_raw{i,1}.specs(:,:,1,1));xlim([1 4]);    % chris w
xlabel('Frequency (ppm)');
ylabel('Amplitude (a.u.)');
title('Multi-channel (water supp.) data before phase correction');
subplot(2,1,2);
plot(out_raw{i,1}.ppm,spec_pre(:,:,1,1));xlim([1 4]);       % chris w
xlabel('Frequency (ppm)');
ylabel('Amplitude (a.u.)');
title('Multi-channel (water supp.) data after phase correction');


if exist(filename2)
    figure('position',[0 500 560 420]);
    subplot(2,1,1);
    plot(out_w_raw{i,1}.ppm,out_w_raw{i,1}.specs(:,:,1,1));xlim([4 5]);
    xlabel('Frequency (ppm)');
    ylabel('Amplitude (a.u.)');
    title('Multi-channel (water unsupp.) data before phase correction');
    subplot(2,1,2);
    plot(out_w_raw{i,1}.ppm,spec_w_pre(:,:,1,1));xlim([4 5]);
    xlabel('Frequency (ppm)');
    ylabel('Amplitude (a.u.)');
    title('Multi-channel (water unsupp.) data after phase correction');
end
pause;
close all;

%%%%%%%%%%%%%%%%%%%%%OPTIONAL REMOVAL OF BAD AVERAGES%%%%%%%%%%%%%%%%
close all
figure('position',[0 0 560 420]);
plot(out_cc.ppm,out_cc.specs);
xlabel('Frequency (ppm)');
ylabel('Amplitude (a.u.)');
title('Water suppressed spectra (all averages)');
       
out_cc2=out_cc;
nBadAvgTotal=0;
nbadAverages=1;
%rmbadav=input('would you like to remove bad averages?  ','s');  % chris w:
%I dont want to remove bad averages from phantoms
close all;
%if rmbadav=='n' || rmbadav=='N'
    out_rm=out_cc;
    % chris w: I dont want to remove bad averages from phantoms
% else
%     sat='n'
%     while sat=='n'||sat=='N'
%         nsd=input('input number of standard deviations.  ');
%         iter=1;
%         nbadAverages=1;
%         nBadAvgTotal=0;
%         out_cc2=out_cc;
%         while nbadAverages>0;
%             [out_rm,metric{iter},badAverages]=op_rmbadaverages(out_cc2,nsd,'t');
%             badAverages
%             nbadAverages=length(badAverages);
%             nBadAvgTotal=nBadAvgTotal+nbadAverages
%             out_cc2=out_rm;
%             iter=iter+1;
%             pause
%             close all;
%         end
%         figure('position',[0 0 560 420]);
%         subplot(1,2,1);
%         plot(out_cc.ppm,out_cc.specs);xlim([1 5]);
%         xlabel('Frequency (ppm)');
%         ylabel('Amplitude (a.u.)');
%         title('Before removal of bad averages:');
%         subplot(1,2,2);
%         plot(out_rm.ppm,out_rm.specs);xlim([1 5]);
%         xlabel('Frequency (ppm)');
%         ylabel('Amplitude (a.u.)');
%         title('After removal of bad averages:');
%         figure('position',[0 500 560 420]);
%         plot([1:length(metric{1})],metric{1},'.r',[1:length(metric{iter-1})],metric{iter-1},'x');
%         xlabel('Scan Number');
%         ylabel('Unlikeness metric (a.u.)');
%         title('Unlikeness metrics before and after bad averages removal');
%         legend('before','after');
%         legend boxoff;
%         sat=input('are you satisfied with bad averages removal? ','s');
%     end
% end

%write a readme file to record the number of dropped avgs
fid=fopen([filestring '/readme.txt'],'w+');
fprintf(fid,'Original number of averages: \t%5.6f',out_raw{i,1}.sz(out_raw{i,1}.dims.averages));   % chris w
disp(['Original number of averages:  ' num2str(out_raw{i,1}.sz(out_raw{i,1}.dims.averages))]);
fprintf(fid,'\nNumber of bad Averages removed:  \t%5.6f',nBadAvgTotal);
disp(['Number of bad averages removed:  ' num2str(nBadAvgTotal)]);
fprintf(fid,'\nNumber of remaining averages in processed dataset:  \t%5.6f',out_rm.sz(out_rm.dims.averages));
disp(['Number of remaining averages in processed dataset:  ' num2str(out_rm.sz(out_rm.dims.averages))]);
fclose(fid);
%%%%%%%%%%%%%%%%%%%%END OF BAD AVERAGES REMOVAL%%%%%%%%%%%%%%%%%%%%



%now align averages;
condition1=true;

while condition1 %% chris w: I edited the code here to stop code crashing when forgetting to put a string in 

    % comment out to make pipline faster
    %driftCorr=input('Would you like to perform the frequency drift correction?  ','s');
    driftCorr='y';
    
    if strlength(driftCorr)==0
        condition1=true;
    else
        condition1=false;
    end
end
    
if driftCorr=='n'|| driftCorr=='N'
    out_aa=out_rm;
    
    if exist(filename2)
        out_w_aa=out_w_cc;
    end
else
    sat='n'
    while sat=='n' || sat=='N'
        out_rm2=out_rm;
        fsPoly=100;
        phsPoly=1000;
        fsCum=zeros(out_rm2.sz(out_rm2.dims.averages),1);
        phsCum=zeros(out_rm2.sz(out_rm2.dims.averages),1);
        iter=1;
        while (abs(fsPoly(1))>0.001 || abs(phsPoly(1))>0.01) && iter<iterin
            close all
            if aaDomain=='t' || aaDomain=='T'
                tmax=input('input tmax for drift correction: ');
                [out_aa,fs,phs]=op_alignAverages(out_rm2,tmax,'n');
            elseif aaDomain=='f' || aaDomain=='F'
                %tmax=input('input tmax for drift correction: ');
                tmax=tmaxin+0.04*randn(1);
                %fmin=input('input fmin for drift correction: ');
                fmin=1.8+0.04*randn(1);
                %fmax=input('input fmax for drift correction: ');
                fmaxarray=[3.5,3.5,4.2,4.2,7.5,4.2];
                fmax=fmaxarray(randi(6,1))
                [out_aa,fs,phs]=op_alignAverages_fd(out_rm2,fmin,fmax,tmax,'n');
            end
            if exist(filename2)
                [out_w_aa,fs_w,phs_w]=op_alignAverages(out_w_cc,5*tmax,'n');
                %[out_w_aa,fs_w,phs_w]=op_alignAverages(out_w_aa,0.5,'n');
            end
            
            fsCum=fsCum+fs;
            phsCum=phsCum+phs;
            
            
            fsPoly=polyfit([1:out_aa.sz(out_aa.dims.averages)]',fs,1)
            phsPoly=polyfit([1:out_aa.sz(out_aa.dims.averages)]',phs,1)
            iter
            
            
            out_rm2=out_aa;
            if exist(filename2);
                out_w_cc=out_w_aa;
            end
            
            
            iter=iter+1;
        end
        %Now plot the cumulative frequency drift correction:
        figure('position',[0 500 1125 420]);
        subplot(2,1,1);
        plot(out_rm.ppm,out_rm.specs);xlim([0 6]);
        xlabel('Frequnecy (ppm)');
        ylabel('Amplitude (a.u.)');
        title('Before drift correction:');
        subplot(2,1,2);
        plot(out_aa.ppm,out_aa.specs);xlim([0 6]);
        xlabel('Frequency (ppm)');
        ylabel('Amplitude (a.u.)');
        title('After drift correction:');
        
        figure('position',[0 0 560 420]);
        plot([1:out_aa.sz(out_aa.dims.averages)],phsCum);
        xlabel('Scan Number');
        ylabel('Phase Drift (deg.)');
        title('Estimated Phase Drift');
        
        figure('position',[570 0 560 420]);
        plot([1:out_aa.sz(out_aa.dims.averages)],fsCum);
        xlabel('Scan Number');
        ylabel('Frequency Drift (Hz)');
        title('Estimated Frequency Drift');
        
        %%chris w: reformat code to stoop in crashing when I dont enter a
        %%string (same as above - line 196)
        condition2=true; 
        
        while condition2
        % chris W : comment out below to make pipline faster
        sat=input('Are you satisfied with the frequency drift correction? ','s');
        %sat='y';
        
            if strlength(sat)==0
                condition2=true;
            else
                condition2=false;
            end
        end
        
    end
    
end
close all;
%now do the averaging and leftshift to get rid of first order phase:
out=op_leftshift(op_averaging(out_aa),out_aa.pointsToLeftshift);
if exist(filename2)
    out_w=op_leftshift(op_averaging(out_w_aa),out_w_aa.pointsToLeftshift);
end

%chris w: only phase the first TE 

if i==1
SpecTool(out,0.05,-2,7);
end

%chris w: input the same 0 order phase for the rest of the TE increment
    %s - I can't work out how to do this atm so I will just hard code the
    %same 0th order phase 

    
    % chris W: commented out to make processing faster  
    %ph0=input('input 0 order phase correction: ');
    %ph1=input('input 1st order phase correction: ');
%ph0=input('input 0 order phase correction: ');
%ph1=input('input 1st order phase correction: ');

        
%chris w: look at phase of first TE by un commenting the above input.
    %once 0th order pahse is determined re run code with that 0th
    %orderphase hard coded
ph0=97.5;  %123.75; 
ph1=0.00115;  %0.0012;

out=op_addphase(out,ph0,ph1);
out_noproc=op_addphase(out_noproc,ph0,ph1);

if exist(filename2)
    
    if i==1
    SpecTool(out_w,0.05,-2,7);
    end
    
    %chris w: input the same 0 order phase for the rest of the TE increment
    %s - I can't work out how to do this atm so I will just hard code the
    %same 0th order phase  
    
    
    % chris W: commented out to make processing faster  
    %ph0=input('input 0 order phase correction: ');
    %ph1=input('input 1st order phase correction: ');
    
    %chris w: look at phase of first TE by un commenting the above input.
    %once 0th order pahse is determined re run code with that 0th
    %orderphase hard coded 
    ph0=7.5; 
    ph1=0; 
    
    out_w=op_addphase(out_w,ph0,ph1);
    out_w_noproc=op_addphase(out_w_noproc,ph0,ph1);
    
end


%chris w: code below is commented out as it is not relivent for me, could
% look into making it into profit readable file.
%wrt=input('write? ','s');
%if wrt=='y' || wrt=='Y'
   % RF=io_writelcm(out,[filestring '/' filestring '_lcm'],out.te);
   % RF=io_writelcm(out_noproc,[filestring '/' filestring '_lcm_unprocessed'],out_noproc.te);
  %  if exist(filename2)
      %  RF=io_writelcm(out_w,[filestring '_w/' filestring '_w_lcm'],out_w.te);
       % RF=io_writelcm(out_w_noproc,[filestring '_w/' filestring '_w_lcm_unprocessed'],out_w_noproc.te);
    %end
%end

%chris w: eddy current correction 
%dataCheck = char(out_w.seq); 
%if ~(strcmp(dataCheck, '%SiemensSeq%\svs_se')) % chris w 03.07.19 : check if data is alex L's of mine (if mine do op_ecc if not don't) - alex uses svs_se for water reference where as I use unsupressed cosy
%[out_ecc,out_w_ecc]=op_ecc_cw(out,out_w);
%out=out_ecc;
%out_w=out_w_ecc;
%end

%chris w -end


%chris w: cell containing all relevant data (processed and unprocessed)from all measurements.
%chris w: Fill in data structure 'relivant_data' - still need to remove
%over sampling

relivant_data{i,1}.out=out;
relivant_data{i,1}.out_w=out_w;
relivant_data{i,1}.out_noproc=out_noproc;
relivant_data{i,1}.out_w_noproc=out_w_noproc;

% Chris W : remove the water signal of each TE inc
relivant_data{i,1}.out=op_removeWater(relivant_data{i,1}.out,[4.4 5],20,1500,1);
% Chris W  : end of water removal  

% chris w: remove over sampling for all output data sets - down sample by a factor of 2
relivant_data{i,1}.out=op_downsamp(relivant_data{i,1}.out,2); 
relivant_data{i,1}.out_w=op_downsamp(relivant_data{i,1}.out_w,2);
relivant_data{i,1}.out_noproc=op_downsamp(relivant_data{i,1}.out_noproc,2);
relivant_data{i,1}.out_w_noproc=op_downsamp(relivant_data{i,1}.out_w_noproc,2);

% chris W: reference each averaged water supressed TE increment to the Cr
% signal (to ref Cr to 3ppm I need to put 3.16 into op_ppmref.m - I can't
% work out why
relivant_data{i,1}.out=op_ppmref(relivant_data{i,1}.out,2.8,2.9,3.16); 


end % chris W: end of loop for each measurement




