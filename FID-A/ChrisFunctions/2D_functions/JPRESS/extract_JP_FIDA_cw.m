%this function extracts FID-A processed JPRESS data and stores it in an array
%This allows me to plot the 2D specrtum in matlab 

%It also applies a the linear phase shift of tau(t1,f2)=1/2*t1*f2 if
%aquired data is from a max echo sampled JPRESS sequence (see et. al. Schulte 2006 for details)


function [FIDA_data_t2t1,FIDA_data_f2t1,FIDA_data_f2f1,ppm2,freq1] = extract_JP_FIDA_cw(relivant_data,NRep,inc,BW) 
% relivant_data - each individual FID-A TE is stored in FID-A file format
% NRep - number of repetitions/measurments 
% inc - increment in ms

FIDA_data_t2t1=ones(2048,NRep);

for i=1:NRep
    FIDA_data_t2t1(:,i)=relivant_data{i,1}.out.fids; % for cosy remove out
end


%is this data from max echo sampled JPRESS sequence 
jp_type=input('Is this max echo sampled JPRESS data? ','s');
if jp_type=='y' || jp_type=='Y'
    
FIDA_data_f2t1= fftshift(ifft(FIDA_data_t2t1,[],1),1);

linPhaseCorrVec=ones(NRep,1);


    for i=1:NRep  % construct phase correction vector 
        % t1 = TE-TEmin  i.e the increment of the echo
        t1= (inc*10^-3)*(i-1); 

        %f2 = BW (bandwidth/sampling frequency) 
        f2=BW;
        
        tau=1/2*t1*f2*(2*pi); % do i need the 2pi in there?? 
        
        linPhaseCorrVec(i,1)=tau;
        
        %now apply the phase correction to the FIDA_data_f2t1 data matrix 
        FIDA_data_f2t1(:,i)=FIDA_data_f2t1(:,i).*exp(1i*tau*pi/180); 
    end
    

else % then it must be half echo sampled JPRESS data
FIDA_data_f2t1= fftshift(ifft(FIDA_data_t2t1,[],1),1);
end


FIDA_data_f2f1= fftshift(ifft(FIDA_data_f2t1,[],2),2);

ppm2=relivant_data{1,1}.out.ppm;   % for cosy remove out

f1_bw = 1/(inc*10^-3); % f1 band width
 
freq1=linspace(-(f1_bw/2), f1_bw/2, NRep);  % scale for the indirect f1 dimension 



