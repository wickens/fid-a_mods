% Chris Wickens: Fucntion that produces a summed projection onto the 
% t2 dimension of Jpress 

function [out_sum] = JP_SumProj(JP_data, reps)
 
for i=1:reps
    
    % For the first iteration JP_data is added to an empty struct - This 
    % allows for summing multiple spectra 
    if i==1
            in1=struct();
            out_sum = op_addScans_abs(in1,JP_data{i,1}.out);
    end
    
    %Itteratively add 2D spectra in JP_data
    out_sum = op_addScans_abs(out_sum, JP_data{i,1}.out);
end