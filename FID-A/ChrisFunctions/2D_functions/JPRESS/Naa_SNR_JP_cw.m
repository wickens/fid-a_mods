
% Chris W: this function calculates the SNR of the NAA peak of all TE
% increments. Not that wheather the input data is siemens or FID-A processed 
%it should be data from the exact same scan (when compairing the two pipe lines)

% relivant_data is in a cell array of TE increments where each cell (or TE) is in FID-A file struture
% NRep is number of repetitions/measurements/increments 

function [all_SNR,all_FWHM,Siemens_FID_A_Struct]=Naa_SNR_JP_cw(in_JP, NRep)

all_SNR=ones(NRep,1);
all_FWHM=ones(NRep,1);
array_size=size(in_JP);
array_size_if_FID_A=[NRep 1];

if array_size==array_size_if_FID_A % If I input a cell array were each cell contains FID-A file format 

    
    
    for i=1:NRep

        in=in_JP{i,1}.out;

        [FWHM,SNR]=run_getLWandSNR_cw(in);

        all_SNR(i,1)=SNR;
        all_FWHM(i,1)=FWHM;
     end
    
else   %otherwise the data strucure must be in siemens processed and just a complex matrix
    
   % load FID-A structure (at this stage it contains data processed by fID-A  
   load ('C:\Users\My PC\OneDrive\Cambridge\JPRESS\JP_Prisma\TWIX\phatom\20181114_JPRESS_TWIX_cw\FID_A_processed\relivant_data.mat');
  
   FID_A_data=relivant_data;
   
   clear relivant_data 
   
   %Delete the all FID-A processed spectroscopic data inside the structure 
   for i=1:NRep
        FID_A_data{i,1}.out.fids=[];   
        FID_A_data{i,1}.out.specs=[];  
   
   end
   
   FID_A_shell=FID_A_data;
   
   %now incert data from siemens processed pipeline - complex sruct from dicomtojcamp  
    
   
   Siemens_t2t1=in_JP;
   
   Siemens_t2t1=Siemens_t2t1.';
   % remeber when I get data from siemens pipe line the spectra is always
   % inverted alony f2 - Thus I need to flip the data set
   Siemens_t2t1=flipud(Siemens_t2t1);
   
   fids_size=size(Siemens_t2t1);
   fids_size=fids_size(1,1);
   
   % Put each siemens FID increment into its own FID-A structure
   for i=1:NRep
   FID_A_shell{i,1}.out.fids=ones(fids_size,1);
   FID_A_shell{i,1}.out.fids=Siemens_t2t1(:,i);
   end
   
   % this cell array now contains data after ICE processed all TE
   % increments - However it is now stored in FID-A .mat file format 
   Siemens_FID_A_Struct=FID_A_shell;
   
   % fft the t2 dimension of my FID matrix
   
   siemens_f2t1=fftshift(ifft(Siemens_t2t1,[],1),1);
   specs_size=fids_size;
   
   for i=1:NRep
   Siemens_FID_A_Struct{i,1}.out.specs=ones(specs_size,1);
   Siemens_FID_A_Struct{i,1}.out.specs=siemens_f2t1(:,i);
   end
   
   % now compute Naa SNR's and water FWHM's
   for i=1:NRep

        in=Siemens_FID_A_Struct{i,1}.out;

        [FWHM,SNR]=run_getLWandSNR_cw(in);

        all_SNR(i,1)=SNR;
        all_FWHM(i,1)=FWHM;
    
    end
   
end

end

   
   

