% op_2DcontrPlt.m   
% Chris Wickens, The university of Cambridge 2020.
%
% DESCRIPTION: 
% Recieves data in 2D FID-A style structure and plots a 2D contour plot.  
% The lower and upper limit of the plot are defined by a predefined factor
% of standard diviation above the noise level
% 
%
% USAGE: out = op_2DcontrPlt(in_2D, min_level, max_level, tit, xppm_min, xppm_max, yppm_min, yppm_max)
%
% INPUTS: 
% in_2D      = data stored in 2D FID-A structure       
% min_level  = number of standard devitions above basline noise for lowest contour      
% min_level  = number of standard devitions above basline noise for highest contour      
% title      = title (string)
% xppm_min   = min xppm (defautl = 1)
% xppm_max   = max xppm (defautl = 4.75)
% yppm_min   = min yppm (defautl = 1)
% yppm_max   = max yppm (defautl = 4.75)
%
% OUTPUTS:
% out = Figure handle for contor plot 

function out = op_2DcontrPlt(in_2D, min_level, max_level, tit, xppm_min, xppm_max, yppm_min, yppm_max)
if nargin<7
    yppm_min =  1; 
    yppm_max = 4.75;
    if nargin<5
        xppm_min = 1;
        xppm_max = 4.75;
        if nargin<4
            tit = '';
            if nargin<3
                max_level = 75;
                if nargin<2
                    min_level = 4;
                end
            end
        end
    end
end


%% retrieve relivant Data:
f2f1 = in_2D.f2f1';  % IMPORTANT: NB this is so ppm1 is on vertical axis
%% retrieve relivant parameters:
ppm2 = in_2D.ppm2;
ppm1 = in_2D.ppm1;


%%
ppm1_noise_min = 1;
ppm1_noise_max = 1.5;

ppm2_noise_min = -2;
ppm2_noise_max = -1;

noisewindow = f2f1(ppm1>ppm1_noise_min & ppm1<ppm1_noise_max, ppm2>ppm2_noise_min & ppm2<ppm2_noise_max);

abs_noisewindow = abs(noisewindow); % absolute noise signal 

mean_noise = mean(abs_noisewindow(:)); % means noise
sd_noise = std(abs_noisewindow(:)); % mean standard deviation 

lower_lim = mean_noise+(min_level*sd_noise);
upper_lim = mean_noise+(max_level*sd_noise);

%% plot using noise threshold
figure();
out = contour(ppm2,ppm1,abs(f2f1),[lower_lim:0.01*10^-9:upper_lim]);  
%out = contour(ppm2,ppm1,abs(f2f1),[lower_lim:1.5*10^-9:upper_lim]);  
%out = contour(ppm2,fliplr(ppm1),abs(f2f1),[0.035*10^-8: 0.04*10^-9: 0.30*10^-8]);
%%out = contour(ppm2,ppm1,abs(f2f1),[0.035*10^-8: 0.04*10^-9: 0.30*10^-8]);   % high resolution - use for thesis
%out = contour(ppm2,ppm1,abs(f2f1),[0.12*10^-8: 0.02*10^-9: 0.35*10^-8]);   % high resolution - use for thesis
%out = contour(ppm2,ppm1,abs(f2f1),[0.035*10^-8: 1.5*10^-9: 0.30*10^-8]);    % low resolution
%out = contour(ppm2,ppm1,abs(f2f1),[0.018*10^-8: 0.04*10^-9: 0.30*10^-8]);   % high resolution - use for thesis use when LB and noise added to mimic invivo data of belinda

colormap('hot')
xlim([xppm_min xppm_max]);  
ylim([yppm_min yppm_max]);

title(tit)
xlabel('ppm2','FontSize',20);
ylabel('ppm1','FontSize',20);
ax = gca;

ax.YAxis.FontSize = 13;
ax.XAxis.FontSize = 13;
%view(-180,90); 
view(-180,-90);   %% For same orientation as Verma



