% op_sineBell.m   
% Chris Wickens, The university of Cambridge 2020.
%
% DESCRIPTION: 
% Multiplies 2D fid by a sine bell filter in both dimensions
% This function is mainly for the out put of my 2D extracting function 
% e.g(extract_cosy_FIDA_cw.m & extract_JP_FIDA_cw.m)
% 
%
%
% INPUTS: 
% beta         =     When beta = 0; sine bell & when beta = 1; cosine-bell
% 2dData       =     Input - unfiltered fid
% inc          =     t1 increment in ms 
% zpFactor_t2  = Zero padding factor in the direct dimension
% zpFactor_t1  = Zero padding factor in the in direct dimension
%
%
% OUTPUTS:
% out       = Sine bell filtered 2D spectrum with zero filling by factor 



function [out] = op_sineBell(in_2d, beta_t1, beta_t2, zpFactor_t2, zpFactor_t1)
    %%
    if ~in_2d.flags.Is2DMRS
        error('ERROR:  This data structure does not support 2D MRS!  Aborting!');
    end

    if in_2d.flags.SineBell
        cont=input('WARNING:  Zero padding has already been performed!  Continue anyway?  (y or n)','s');
        if cont=='y'
            %continue;
        else
            error('STOPPING');
        end
    end

    
    % retrieve relivant parameters:
    f2_bw = in_2d.spectralwidth;    % Spectral width in direct dimension 
    inc = in_2d.inc;                %  t1 inc (seconds)
    
    % get size of 2D data set
    sz = in_2d.sz;   % sz(1) = data points in t2 and sz(2) = data points in t1
    
    t2 = in_2d.t2;
    t1 = in_2d.t1;
    
  
   %%
    % Make the sine bell window funtion to for filtering the fid
    % relivant equation from:
    % 'https://www.lifesci.sussex.ac.uk/nmr/docs/UserManual.pdf' - page 178
    % fid_sb = sin(beta+[pi-beta]*.t/tmax)
    
    dk_t1 = sin(beta_t1+((pi-beta_t1).*t1/t1(end))); % dk_t1: function for indirect dimension
    dk_t2 = sin(beta_t2+((pi-beta_t2).*t2/t2(end))); % dk_t2: function for direct dimension
    
    %%
    % Multiply the dk_t1 windowfunction along the t1 axis 
    fid2fid1_noProc = in_2d.fid2fid1;   % get 2D time domain data 
    fid2fid1_proc = ones(sz(1),sz(2));
        for i=1:sz(1)
            fid2fid1_proc(i,:) = fid2fid1_noProc(i,:).*dk_t1;
        end
        clear fid2fid1_noProc
        
    % Multiply the dk_t2 windowfunction along the t2 axis
    
       dk_t2=dk_t2.';  % transpose
        for i=1:sz(2)
            fid2fid1_proc(:,i) = fid2fid1_proc(:,i).*dk_t2;
        end
    
    %calculate how many zeros to add in each dimension
    
    zp2 = ceil((sz(1)*zpFactor_t2)-sz(1));
    zp1 = ceil((sz(2)*zpFactor_t1)-sz(2));

    %Add zeros using MATLAB array zeropadding function;
    fid2fid1_proc = padarray(fid2fid1_proc,[zp2,zp1],0,'post');  
    
    %recalculate the sz vector
    sz_zp = size(fid2fid1_proc);
    
    %% perform FT alnong both dimensions
    
    f2fid1_proc = fftshift(ifft(fid2fid1_proc,[],1),1); % st = dims = spec & time
    f2f1_proc = fftshift(ifft(f2fid1_proc,[],2),2);  % s = both dims = spec
    
    % sine bell filtered and zerofilled 2D data:
    fid2fid1 = fid2fid1_proc;
    f2fid1 = f2fid1_proc;  
    f2f1 = f2f1_proc; 
    
    clear fid2fid1_proc f2fid1_proc f2f1_proc
   
    %%
    % Now re-calculate t2 and ppm2 arrays using the calculated parameters:
    f2 = [(-f2_bw/2)+(f2_bw/(2*sz_zp(1))):f2_bw/(sz_zp(1)):(f2_bw/2)-(f2_bw/(2*sz_zp(1)))];
    ppm2 = -f2/(in_2d.Bo*42.577);
    ppm2 = ppm2+4.65;
    t2 = [0:in_2d.dwelltime:(sz_zp(1)-1)*in_2d.dwelltime];
    
    % Now re-calculate t1 and ppm1 arrays using the calculated parameters:
    f1_bw = 1/inc; % Hz
    
    f1 = [(-f1_bw/2)+(f1_bw/(2*sz_zp(2))):f1_bw/(sz_zp(2)):(f1_bw/2)-(f1_bw/(2*sz_zp(2)))];
    ppm1 = -f1/(in_2d.Bo*42.577);  
    ppm1 = ppm1+4.56;
    t1 = [0:inc:(sz_zp(2)-1)*inc];
    ppm1 = fliplr(ppm1); % flip the ppm1 axis left to right. Important for subsequent 2D operations (e.g. ploting and volume analysis)
   
    %%
    %FILLING IN DATA STRUCTURE
    out = in_2d;
    
    out.fid2fid1 = fid2fid1;
    out.f2fid1 = f2fid1;  
    out.f2f1 = f2f1;
    
    out.t1 = t1;
    out.t2 = t2;
    out.ppm1 = ppm1;
    out.ppm2 = ppm2; 
    out.sz = sz_zp;
      
    %FILLING IN THE FLAGS
    out.flags.SineBell = 1;
    
end

