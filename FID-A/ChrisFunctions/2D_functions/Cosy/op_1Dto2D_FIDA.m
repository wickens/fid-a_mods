% op_1Dto2D_FIDA.m
% Chris Wickens, The university of Cambridge 2020.
%
% DESCRIPTION: 
% This function extracts 2D data strored in multiple 1D FIDA structures  
% and stores it an a signle FIDA style structure that suports the extra dimension
% of 2D MRS. Subsequent 2D MRS functions take this new 2D FIDA style 
% structre as an input to perform their operations. 
% 
%
%
% INPUTS: 
% relivant_data     = each individual FID-A TE is stored in FID-A file format
% inc               = Size of the TE increment (ms)
%
% OUTPUTS:
% out               = Input dataset in 2D FID-A style structure format.


function [out] = op_1Dto2D_FIDA(relivant_data,inc) 

%% retrieve relivant parameters:
NRep = size(relivant_data);
NRep = NRep(1);   % NRep = number of repetitions/measurments 
vec = relivant_data{1,1}.out.sz(1); % vec = number of data points per aquisition 

FIDA_data_t2t1=ones(vec,NRep);   
sz = size(FIDA_data_t2t1);

inc = inc*10^-3;    % convert increment (inc) into seconds

%% Store data in a 2D array: FIDA_data_t2t1
for i=1:sz(2)
    FIDA_data_t2t1(:,i)=relivant_data{i,1}.out.fids; 
end

FIDA_data_f2t1= fftshift(ifft(FIDA_data_t2t1,[],1),1); % ft along t2
FIDA_data_f2f1= fftshift(ifft(FIDA_data_f2t1,[],2),2); % ft along t1

ppm2=relivant_data{1,1}.out.ppm;   % for cosy remove out direct dimension 

%% calculate t1 and ppm1 arrays:

% calculate PPM1
f1_bw = 1/(inc); % f1 band width
%freq1=linspace(-(f1_bw/2), f1_bw/2, sz(2));  % scale for the indirect f1 dimension 
freq1=[(-f1_bw/2)+(f1_bw/(2*sz(2))):f1_bw/(sz(2)):(f1_bw/2)-(f1_bw/(2*sz(2)))];

% convert the f1 dimension in cosy to ppm by dividing by B0
Bo = relivant_data{1,1}.out.Bo; % in Tesla 
ppm1=(-freq1/(Bo*42.577))+4.75;% f/1H gyro magnetic ratio (f/42.577 MHz) - indirect dimension 
ppm1 = fliplr(ppm1); % flip the ppm1 axis left to right. Important for subsequent 2D operations (e.g. ploting and volume analysis)

% calc t1 dimension 
%t1 = linspace(0,(sz(2)-1)*(inc),sz(2));
t1 = [0:inc:(sz(2)-1)*(inc)];


%% CONSTRUCT 2D MRS DATA STRUCTURE
% copy paramter information from the first TE increment data:
out.averages = relivant_data{1,1}.out.averages;
out.Bo = relivant_data{1,1}.out.Bo;
out.date = relivant_data{1,1}.out.date; 
out.dwelltime = relivant_data{1,1}.out.dwelltime;
out.pointsToLeftshift = relivant_data{1,1}.out.pointsToLeftshift;
out.rawAverages = relivant_data{1,1}.out.rawAverages;
out.rawSubspecs = relivant_data{1,1}.out.rawSubspecs;
out.seq = relivant_data{1,1}.out.seq;
out.spectralwidth = relivant_data{1,1}.out.spectralwidth;
out.subspecs = relivant_data{1,1}.out.subspecs;
out.te = relivant_data{1,1}.out.te;
out.tr = relivant_data{1,1}.out.tr;
out.txfrq = relivant_data{1,1}.out.txfrq;
out.watersupp = relivant_data{1,1}.out.watersupp;
% chris W: added on 30/10/2020
if isfield(relivant_data{1,1}.out, 'trm_fct')
    out.trm_fct = relivant_data{1,1}.out.trm_fct;
end
% fields that need to be modified and created to accomidate 2D frequency
% FID-A structure:
out.sz = sz; 
out.fid2fid1 = FIDA_data_t2t1;  % both dimensions in time domain
out.f2fid1 = FIDA_data_f2t1;    % 2nd and 1st dimension in freq and time, respectively 
out.f2f1 = FIDA_data_f2f1;      % both dimensions in freq domain
out.inc = inc;                  % TE increment in seconds
out.t1 = t1;
out.t2 = relivant_data{1,1}.out.t;
out.ppm1 = ppm1;
out.ppm2 = ppm2;
out.flags = relivant_data{1,1}.out.flags;   % maybe updat this
out.flags.Is2DMRS = 1;   % flag to tell 2D functions that this is in a 2D FIDA style structure
out.flags.SineBell = 0;   % has sine bell filtering been applied 0 = no & 1 = yes
out.dims = relivant_data{1,1}.out.dims;    

