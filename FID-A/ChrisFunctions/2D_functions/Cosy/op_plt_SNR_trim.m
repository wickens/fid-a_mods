% op_plt_SNR_trim
% Chris Wickens, The university of Cambridge 2020.
% 
% 
% DESCRIPTION: 
% This funciton plots the SNR of each metabolite as a
% function of whterever is varing between the processing cosy data - for 
% example different trimming of the fid input
% 
% 
% USAGE: 
% [out] = op_plt_SNR_trim(in, crosOrDiag, SNROrVolInt)
%
%
% INPUTS:
% in           = cell containing one or multiple 2D FIDA strucutre data sets with anaysis stored in 
%                VolInt_SNR
% crosOrDiag   = diagonal or cross peak (options: 1 = diagonal peaks or 2 = cross peaks)
% SNROrVolInt  = SNR or volume integral (options: 1 = SNR or 2 = VolInt)
% 
% OUTPUTS:
% out   = plot of SNR 
%       
% 
%
%


function [out] = op_plt_SNR_trim(in, crosOrDiag, SNROrVolInt)

switch crosOrDiag
    case 1
        peak_typ = char('diagonal');
    case 2
        peak_typ = char('cross');
end

switch SNROrVolInt
    case 1
        anal_Type = char('SNR');
    case 2
        anal_Type = char('VolInt');
end
   
% get trimming factor  (trimming factor = trm_fct)
trm_fct = ones(1, numel(in));

for i=1:numel(in)
    trm_fct(1,i) = in{i,1}.trm_fct;
end



% get peak names
metabs = fieldnames(in{1,1}.VolInt_SNR.(peak_typ));

sz_metabs = numel(metabs);


% pre-alocate space for y(trimming factor, metabolite)
y = ones(numel(trm_fct), sz_metabs);

for i=1:numel(trm_fct)
    for ii=1:sz_metabs
        y(i,ii) = in{i,1}.VolInt_SNR.(peak_typ).(metabs{ii}).(anal_Type);
    end
end
clear i ii


% generate axis for plot 
figure()
x = trm_fct; % 
% make x axis in usnit of killo points
x=x/1000;

%% plot
lineStyles=linspecer(numel(metabs), 'qualitative');

%out = plot(x,y,'LineWidth',2);
for i=1:numel(metabs)
    hold on
   
    
    out = plot(x,y(:,i),'color',lineStyles(i,:),'linewidth',2);

end
x_lab = 'Number of points trimmed (K)';
ax = gca; 
xlabel(x_lab, 'FontSize', 18)
ax.XAxis.FontSize = 14;
xtickangle(45)

%{
xTick = get(gca,'XTickLabel');
set(gca, 'XTickLabel', xTick, 'fontsize', 11)

xtickangle(45)
%}


y_lab = sprintf('SNR (%s pks.)',peak_typ);
ylabel(y_lab, 'FontSize', 18)
ax.YAxis.FontSize = 14;

leg = legend(metabs, 'Location','NorthEastOutside');
set(leg,'Interpreter', 'none');
leg.FontSize = 14;
box off


end