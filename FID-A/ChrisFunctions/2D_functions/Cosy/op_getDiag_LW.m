% op_getDiag_LW.m
% Chris Wickens, The university of Cambridge 2020.
% 
% 
% DESCRIPTION: 
% This function calculates the line widths of the diagonal peaks of a 2D
% AL-Cosy spectrum 
%
% USAGE: 
%
% INPUTS:
% in    = data stored in 2D MRS FIDA strucutre
% 
% 
% OUTPUTS:
% out = A structure 2D FID-A style data containing line widths of specified
%       diagonal peaks
%       
% 

function [LW, out] = op_getDiag_LW(in)

% ppm1 slice ppm1_val [Naad_ppm1; Crd_ppm1; Chod_ppm1; mld_ppm1]  
ppm1_valArr = [2.0; 3.0; 3.2; 3.5];

% ppm2 range - ppm2_rang

ppm2_rangArr = [1.80 2.20; 2.90 3.10; ...
            3.10 3.30; 3.45 3.57];

% get peak names
x = fieldnames(in{1,1}.VolInt_SNR.diagonal);


for i = 1:length(ppm1_valArr)
    freq_est = ppm1_valArr(i);
    ppm2_rang = ppm2_rangArr(i, :);   

    % extract slice from 2D data
    [out] = op_freqSlice_2D(in, freq_est, ppm2_rang);
    metabLab = out{1,1}.metabLab;
    
    LW.(metabLab) = ones(numel(out),1); % stroe LW data here
   for ii=1:numel(out)
    parsFit=op_Fit1D(out{ii},0,0,ppm2_rangArr(i,1),ppm2_rangArr(i,2), metabLab);   
    
    %op_plotfid(out{ii},0.6)
    figure(ii)
    plot(out{ii}.t, out{ii}.fids)
    LW.(metabLab)(ii,1) = parsFit(2); % line width
    
   end
    clear i ix_ppm1 d metab_sl
end
    
    
    
end % end of function 