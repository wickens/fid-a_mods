% op_volTemplt_7T_invivo.m   
% Chris Wickens, The university of Cambridge 2020.
%
% DESCRIPTION: 
% For IN VIVO data
% This functions out puts a structure containing target volume regions of 
% both diagonal and cross peaks of key cerebral in vivo metabolites 
% The volume information is stored within labeld metabolite fields. 
% This template is intended for use on in vivo 7T Cosy data.
% 
%
% INPUTS: 
% No inputs required 
%
% OUTPUTS:
% out       = metabolite volume region template

function vol_b = op_volTemplt_7T_invivo();

%% Define volume integrals
% diagonal peaks
vol_b.diag = [];  % structure to store the diagonal peaks 

%Structure vol_b.metabolite = [ ppm2 range ; ppm1 range]
% ---
% can't see in vivo
%vol_b.diag.lac_d = [1.255 1.335 ; 1.125 1.5];
% ---
vol_b.diag.naa_d =  [1.915 2.065 ; 1.82 2.15];   
%vol_b.diag.naa_d =   [1.915 2.065 ; 1.75 2.2]; % test
%vol_b.diag.naa_d =   [1.845 2.135 ; 1.75 2.2]; % test
%vol_b.diag.naa_d =   [1.75 2.25 ; 1.75 2.2]; % test

% ---
vol_b.diag.Cr_d = [2.95 3.065 ; 2.842 3.24];
% ---
%vol_b.diag.Cho_d = [3.1 3.225 ; 3.0 3.35];
vol_b.diag.Cho_d = [3.15 3.235 ; 3.05 3.35];
% ---
vol_b.diag.ml_d = [3.45 3.55 ; 3.40 3.65];  % not sure if this one is correct


% cross peaks
vol_b.cross = []; % structure to store the diagonal peaks -- _b is short for 'box'

%Structure vol_b.metabolite = [ ppm2 range ; ppm1 range]
% ---
% can't see lactate in vivo  
%vol_b.cross.lac_c_ab = [1.258 1.332 ; 3.85 4.30]; % ab = above
%vol_b.cross.lac_c_bel = [4.025 4.14 ; 1.15 1.47]; % bel = below 
% --- Naa
vol_b.cross.naa_ab = [2.4 2.53 ; 4.17 4.53];
% --- Glu
vol_b.cross.Glu_ab =  [2.058 2.13; 2.21 2.42];% Glu is the glutamate cross peak that does not fully overlap with gln in vivo 
vol_b.cross.Glu_bel = [2.275 2.40 ; 1.85 2.10];  %7 
% -- Gln
vol_b.cross.Gln_ab = [2.075 2.16 ; 2.42 2.60 ];
vol_b.cross.Gln_bel = [2.42 2.50 ; 1.87 2.15];

% ---Glx
vol_b.cross.Glux_ab = [2.085 2.15 ; 3.7 3.95];  % Glux is the glutamate cross peak that overlaps with the Gln signal in vivo 
%vol_b.cross.Glux_bel = [3.69 3.76 ; 1.89 2.25];
% --- mlCho
vol_b.cross.mlCho_ab = [3.45 3.565 ; 3.87 4.24];

% --- ml
vol_b.cross.ml_ab = [3.25 3.325 ; 3.46 3.80];

% --- PC
vol_b.cross.PC = [3.60 3.67 ; 4.195 4.475];

% --- PE
vol_b.cross.PE = [3.25 3.320 ; 3.91 4.15];

% --- Asp
vol_b.cross.Asp = [2.6 2.70 ; 3.8 4.135];

% --- Lys
vol_b.cross.Lys = [1.6 1.715 ; 2.865 3.125];

