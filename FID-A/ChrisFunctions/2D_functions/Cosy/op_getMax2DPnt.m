% op_getMax2DPnt.m   
% Chris Wickens, The university of Cambridge 14/10/2020.
%
% DESCRIPTION: 
% This function is incerted into io_volInt_2D.m and records the ppm of the
% max peak in the given volume integral
%
%
% INPUTS: 
% in       =  absolute data that is in the naa_d pre dfined volume
% ppm1     = ppm1 axis
% ppm2     = ppm2 axis
% ppm1_min = ppm1 lower lim of volume
% ppm1_max = ppm1 upper lim of volume 
% ppm2_min = ppm2 lower lim of volume
% ppm2_max = ppm2 upper lim of volume
%
%
% OUTPUTS:
% out      =  


%function [Naa_mxPPM2, Naa_mxPPM1] = op_getMax2DPnt(in, ppm1, ppm2, ppm1_min, ppm1_max, ppm2_min, ppm2_max)
function [Naa_mxPPM2, Naa_mxPPM1] = op_getMax2DPnt(in, ppm1_min, ppm1_max, ppm2_min, ppm2_max)


% retrieve relivant parameters:
spec_2D_matrix = in.f2f1.';
ppm2 = in.ppm2;
ppm1 = in.ppm1;

sigwindow_d = spec_2D_matrix(ppm1>ppm1_min & ppm1<ppm1_max, ppm2>ppm2_min & ppm2<ppm2_max); 

% ppm axis fo the volum of the metabolite being analysed
ppm1_vol = ppm1(ppm1>ppm1_min & ppm1<ppm1_max);
ppm2_vol = ppm2(ppm2>ppm2_min & ppm2<ppm2_max);

% 
[max_pnt, index] = max(sigwindow_d);
[max_pnt2, index2] = max(max_pnt);

% find indexes of the max point
ppm2Idx = index2;
ppm1Idx = index(index2);

% get ppm cordinate of the max point
Naa_mxPPM1 = ppm1_vol(ppm1Idx);
Naa_mxPPM2 = ppm2_vol(ppm2Idx);


%fprintf('max point in op_getMax2DPnt.m = %d \n', in(ppm1Idx, ppm2Idx))
fprintf('max point ppm1_vol = %d and ppm2_vol = %d \n', Naa_mxPPM1, Naa_mxPPM2);

end 