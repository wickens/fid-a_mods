% Chris W: This function (cosyf1_into_FIDA.m) reorganises the indirect f1 dimension of cosy into 
% FID A file format so the output can be refferenced by op_ppmref.m
% 
% cosyf1_into_FIDA.m uses the out put of extract_cosy_FIDA_cw.m




% op_ppmref_cosy_f1.m
% Jamie Near, McGill University 2015.
% 
% USAGE:
% [out,frqshift]=op_ppmref(in,ppmmin,ppmmax,ppmrefval,dimNum);
% 
% DESCRIPTION:
% Search for the peak located between ppmmin and ppmmax, and then give that
% peak a new ppm reference value.
% 
% INPUTS:
% in        = input data in matlab structure format.
% ppmmin    = minimum of ppm search range.
% ppmmax    = maximum of ppm search range.
% ppmrefval = new reference ppm value.
% dimNum    = which subspectrum to used for referencing (optional).
%
% OUTPUTS:
% out       = Output dataset following frequency shift.
% frqshift  = Frequency shift applied (in Hz).

function [FID_A_ppm1]=cosyf1_into_FIDA(relivant_data, FIDA_data_t2t1,FIDA_data_f2f1,ppm1,inc,NRep);
    
% refference the indirect dimension 

% first I need to extract one of the indirect dimension into fid-a format
% (while correcting the flags)

% 1st use a cell from relivant data and clear the data + correct wrong
% flags
%%

fid_A_cell=relivant_data{1,1}.out;

fid_A_cell.fids=[];
fid_A_cell.specs=[];
fid_A_cell.ppm=[];
fid_A_cell.dewlltime=[];
fid_A_cell.spectralwidth=[];
fid_A_cell.sz=[];

%change inc into 'ms'
inc=inc*10^-3;
t1max=inc*NRep;
t1min=0;

% Gather data from output of extract_cosy_FIDA_cw
fid_A_cell.fids=FIDA_data_t2t1(1,:).';    %fids
fid_A_cell.fids=flipud(fid_A_cell.fids);

fid_A_cell.specs=FIDA_data_f2f1(1,:).';   % specs
fid_A_cell.specs=flipud(fid_A_cell.specs); 

fid_A_cell.ppm=ppm1;                      % ppm
fid_A_cell.t=linspace(t1min,t1max,NRep);   % time in indirect dimension  
fid_A_cell.dwelltime=inc;
fid_A_cell.spectralwidth=(1/inc); % spectralwidth
fid_A_cell.sz=[64,1];
fid_A_cell.flags.zeropadded=1;        % this needs to be 1 so the referenceing code works


FID_A_ppm1=fid_A_cell;


