% Chris w 22.10.2020
% op_batch_proc_2D.m
%
% DESCRIPTION: 
% This functions performs batch processing on several 2D cosy data sets
%
%
% USAGE:
% function [out] = op_batch_proc_2D(in, inc, beta_t1, beta_t2, zpFactor_t2, zpFactor_t1) 
%
% INPUTS: 
% in           = data containing multiple 2D data set or just a single data set 
% inc          = t1 increment in ms 
% beta_t1      = along indirect (t1) dimeWhen beta = 0; sine bell & when beta = 1; cosine-bell
% beta_t2      = along direct (t2) dimeWhen beta = 0; sine bell & when beta = 1; cosine-bell
% zpFactor_t2  = Zero padding factor in the direct dimension
% zpFactor_t1  = Zero padding factor in the in direct dimension
%
%
% OUTPUTS: 
% out = cell containing multiple (or a single) fully processed 2D MRS data



function [out] = op_batch_proc_2D(in, inc, beta_t1, beta_t2, zpFactor_t2, zpFactor_t1) 


% get relivant data
if iscell(in) % enter if using multiple sets of 2D data - that has already been stored in a cell array
    for i=1:numel(in)
        in{i,1} = in{i,1}.relivant_data;
    end
elseif ~iscell(in) % enter if just using a single data set
    in_temp = cell(1,1);
    in_temp{1,1} = in.relivant_data;
    in = in_temp;
    clear in_temp
end
    
in_proc = in;

for i=1:numel(in)
    in_proc{i,1} = op_1Dto2D_FIDA(in{i,1},inc); % extract phatom data
    in_proc{i,1} = op_sineBell(in_proc{i,1}, beta_t1, beta_t2, zpFactor_t2, zpFactor_t1); % filter
    in_proc{i,1}.ppm1 = in_proc{i,1}.ppm1 + 0.2; % reffernce f1 for naa is at 2.0 ppm
    
    %op_2DcontrPlt(in_proc{i,1});
end

out = in_proc;


end % end of function