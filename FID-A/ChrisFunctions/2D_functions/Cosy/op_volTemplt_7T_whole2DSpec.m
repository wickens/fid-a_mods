% op_volTemplt_7T_whole2DSpec.m   
% Chris Wickens, The university of Cambridge 2020.
%
% DESCRIPTION: 
% This functions out puts a structure containing target voume regions of 
% the most of the peaks in the 2D spectrum  
% 
% The volume information is stored within labeld fields. 
% This template is intended fo use on 7T Cosy data.
% 
%
% INPUTS: 
% 
%
% OUTPUTS:
% out       = metabolite volume region template

function vol_b = op_volTemplt_7T_whole2DSpec();

%% Define volume integrals
% diagonal peaks
vol_b.diag = [];  % structure to store the diagonal peaks 

%Structure vol_b.metabolite = [ ppm2 range ; ppm1 range]
% ---
vol_b.diag.lac_d = [1.255 1.335 ; 1.125 1.5];
% ---
%vol_b.diag.naa_d =  [1.915 2.065 ; 1.82 2.2];   
%vol_b.diag.naa_d =   [1.915 2.065 ; 1.75 2.2]; % test
vol_b.diag.naa_d =   [1.845 2.135 ; 1.75 2.2]; % test

% ---
vol_b.diag.Cr_d = [2.95 3.065 ; 2.842 3.24];
% ---
vol_b.diag.Cho_d = [3.1 3.225 ; 3.0 3.35];
% ---
vol_b.diag.ml_d = [3.45 3.55 ; 3.30 3.75];  % not sure if this one is correct

% ---
vol_b.diag.all_metab =   [1.10 4.50; 1.10 4.5]; % SNR of whole spectrum

% cross peaks
vol_b.cross = []; % structure to store the diagonal peaks -- _b is short for 'box'

%Structure vol_b.metabolite = [ ppm2 range ; ppm1 range]
% ---
vol_b.cross.lac_c_ab = [1.258 1.332 ; 3.85 4.30]; % ab = above
vol_b.cross.lac_c_bel = [4.025 4.14 ; 1.15 1.47]; % bel = below 
% ---
vol_b.cross.naa_c_ab = [2.4 2.53 ; 4.17 4.53];
vol_b.cross.naa_c_bel = [4.31 4.41; 2.23 2.9];
% ---
vol_b.cross.Glu_ab =  [2.018 2.13; 2.21 2.5];% Glu is the glutamate cross peak that does not fully overlap with gln in vivo 
vol_b.cross.Glu_bel = [2.275 2.37 ; 1.85 2.17]; 
% ---
vol_b.cross.Glux_ab = [2.025 2.15 ; 3.572 3.91];  % Glux is the glutamate cross peak that overlaps with the Gln signal in vivo 
vol_b.cross.Glux_bel = [3.69 3.76 ; 1.89 2.25];
% ---
vol_b.cross.mlCho_ab = [3.45 3.545 ; 3.87 4.21];
vol_b.cross.mlCho_bel = [3.999 4.075 ; 3.35 3.67];