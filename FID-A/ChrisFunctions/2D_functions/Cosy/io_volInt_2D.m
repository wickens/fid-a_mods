% io_volInt_2D.m
% Chris Wickens, The university of Cambridge 2020.
% 
% 
% DESCRIPTION: 
% This funciton analysise the 2D MRS data set (i.e. volume integrals and SNR)
% Input data would have passed through runpressproc_cw_fast.m (rename this) 
% and have been converted into a 2D FID-A MRS structure.
%            
%
% INPUTS:
% in    = data stored in 2D MRS FIDA strucutre
% 
% ppm2_noise_min = minimum ppm2 region of noise (optional)
% ppm2_noise_max = maximum ppm2 region of noise (optional)
% ppm1_noise_min =  minimum ppm1 region of noise(optional)
% ppm1_noise_max = maximum ppm2 region of noise (optional)
% 
% OUTPUTS:
% out = A structure containing volume integrals, max signal and SNR of
%       of the defined spectral ROI's
% 

function [out] = io_volInt_2D(in, ppm2_noise_min, ppm2_noise_max, ppm1_noise_min, ppm1_noise_max)

if nargin<5  
    ppm1_noise_max = 1.5;
    if nargin<4        
        ppm1_noise_min = 1;
        if nargin<3            
            ppm2_noise_max = -1;
            if nargin<2 
                ppm2_noise_min = -2;        
            end
        end
    end
end


%% retrieve relivant parameters:
spec_2D_matrix = in.f2f1.';
ppm2 = in.ppm2;
ppm1 = in.ppm1;

%% Define volume integrals
vol_b = op_volTemplt_7T();  % Genterate a template for peak volume and SNR anaysis 
%vol_b = op_volTemplt_7T_whole2DSpec();
%vol_b = op_volTemplt_7T_invivo();

%% plot contour plt
% chris W 06102020 : stop aksing this so batch code runs faster (temporarily!)
%cntr_plt = input('Do you want to display a contour plot? ', 's');

%if cntr_plt == 'Y' || cntr_plt == 'y'
    %op_2DcontrPlt(in, 4, 75);
    %op_2DcontrPlt(in, 4, 10);
    %op_2DcontrPlt(in, 4, 10);
%end


%% Creat a structure to store voume integrals
Vol_int = [];
Vol_int.diagonal = [];
Vol_int.cross = [];

%% Get a 2D region of noise

noisewindow = spec_2D_matrix(ppm1>ppm1_noise_min & ppm1<ppm1_noise_max, ppm2>ppm2_noise_min & ppm2<ppm2_noise_max);
abs_noisewindow = abs(noisewindow); % absolute noise signal 

mean_noise = mean(abs_noisewindow(:)); % means noise
sd_noise = std(abs_noisewindow(:)); % mean standard deviation 

nosie_thresh = mean_noise+(3*sd_noise);

Vol_int.noise_thresh = nosie_thresh;
Vol_int.noise_mean = mean_noise;
Vol_int.nosie_sd = sd_noise;

% plot noise region
hold on 
ppm2_noiseROI = [ppm2_noise_min, ppm2_noise_max, ppm2_noise_max, ppm2_noise_min, ppm2_noise_min];
ppm1_noiseROI = [ppm1_noise_min, ppm1_noise_min, ppm1_noise_max, ppm1_noise_max, ppm1_noise_min];
plot(ppm2_noiseROI, ppm1_noiseROI, 'b-', 'LineWidth', 2);

clear ppm1_noiseROI ppm2_noiseROI

%% plot volume boxes on 2D cosy spectra
% diagonal peak

fn_d = fieldnames(vol_b.diag);
for i=1:numel(fn_d)
    ppm_cord = vol_b.diag.(fn_d{i});  % get ppm coordinate for this ROI
    
    ppm2_min = ppm_cord(1,1);
    ppm2_max = ppm_cord(1,2);
    
    ppm1_min = ppm_cord(2,1);
    ppm1_max = ppm_cord(2,2);
    
    % Define the region of the metabolite signal - diagonal peak
    ppm2_dsigROI = [ppm2_min, ppm2_max, ppm2_max, ppm2_min, ppm2_min];
    ppm1_dsigROI = [ppm1_min, ppm1_min, ppm1_max, ppm1_max, ppm1_min];
    
    
    % plot the volume region over the previously plotted contour plot
    hold on 
    plot(ppm2_dsigROI, ppm1_dsigROI, 'b-', 'LineWidth', 2);
    
    
    % For cross peaks calculate volume integral of 2D region of signal in each for loop 
    % iteration 
    sigwindow_d = spec_2D_matrix(ppm1>ppm1_min & ppm1<ppm1_max, ppm2>ppm2_min & ppm2<ppm2_max); 
    abs_sigwindow_d = abs(sigwindow_d); % absoulute signal 
    
    % use threshold calculated above to avoid noise contribution from absolute
    % volume integral calculation
    mask_thresh = abs_sigwindow_d < nosie_thresh;   
    abs_sigwindow_thresh = abs_sigwindow_d(~mask_thresh);
    vol_int_d = sum(abs_sigwindow_thresh);
    
    % Also calculate the signal of the most intense data point and SNR
    MaxSig_d = max(abs_sigwindow_thresh);
    
    if strcmp(string(fn_d{i}), 'naa_d')
        [Naa_mxPPM2, Naa_mxPPM1] = op_getMax2DPnt(in, ppm1_min, ppm1_max, ppm2_min, ppm2_max);
    end
    
    SNR_d = MaxSig_d/Vol_int.nosie_sd; % calc SNR
        
    % store in structure
    Vol_int.diagonal.(fn_d{i}).VolInt = vol_int_d;    % volume integral
    Vol_int.diagonal.(fn_d{i}).MaxSig = MaxSig_d; % singal of most intense peak in volume integral
    Vol_int.diagonal.(fn_d{i}).SNR = SNR_d;     % SNR

    clear ppm2_min ppm2_max ppm1_min ppm1_max ppm2_dsigROI ppm1_dsigROI
end



% cross peaks
fn_c = fieldnames(vol_b.cross);

for i=1:numel(fn_c)
    ppm_c = vol_b.cross.(fn_c{i}); 
    
    ppm2_min = ppm_c(1,1);
    ppm2_max = ppm_c(1,2);
    
    ppm1_min = ppm_c(2,1);
    ppm1_max = ppm_c(2,2);
    
    % Define the region of the metabolite signal - cross peak
    ppm2_csigROI = [ppm2_min, ppm2_max, ppm2_max, ppm2_min, ppm2_min];
    ppm1_csigROI = [ppm1_min, ppm1_min, ppm1_max, ppm1_max, ppm1_min];
    
    
    % plot the volume region over the previously plotted contour plot
    hold on 
    plot(ppm2_csigROI, ppm1_csigROI, 'b-', 'LineWidth', 2);
    
    
    % For cross peaks calculate volume integral of 2D region of signal in each for loop 
    % iteration 
    sigwindow_c = spec_2D_matrix(ppm1>ppm1_min & ppm1<ppm1_max, ppm2>ppm2_min & ppm2<ppm2_max); 
    abs_sigwindow_c = abs(sigwindow_c); % absoulute signal 
    
    % use threshold calculated above to avoid noise contribution from absolute
    % volume integral calculation
    mask_thresh = abs_sigwindow_c < nosie_thresh;   
    abs_sigwindow_thresh = abs_sigwindow_c(~mask_thresh);
    vol_int_c = sum(abs_sigwindow_thresh);
    
    
    % Also calculate the signal of the most intense data point and SNR
    MaxSig_c = max(abs_sigwindow_thresh);
    SNR_c = MaxSig_c/Vol_int.nosie_sd;  % SNR
    
    %% store in structure
    Vol_int.cross.(fn_c{i}).VolInt = vol_int_c; % Volume integral
    Vol_int.cross.(fn_c{i}).MaxSig = MaxSig_c; % singal of most intense peak in volume integral
    Vol_int.cross.(fn_c{i}).SNR = SNR_c;   % SNR
    
    clear ppm2_min ppm2_max ppm1_min ppm1_max ppm2_csigROI ppm1_csigROI
end

% fill in postion of the Naa peak
Vol_int.diagonal.naa_d.Naa_mxPPM2 = Naa_mxPPM2;
Vol_int.diagonal.naa_d.Naa_mxPPM1 = Naa_mxPPM1;

out = Vol_int;
return 



