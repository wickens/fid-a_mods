% op_getDiag_LW.m
% Chris Wickens, The university of Cambridge 2020.
%
% DESCRIPTION:
% This function take 2D mrs is 2D MRS data structe and selects a slice to
% store in a 1D mrs FID-A file structure. This allows the use of FID-A
% functions on that frequence slice
%
%
% USAGE: [out] = op_freqSlice_2D(in, ppm1_val, ppm2_range);
% 
%
% INPUTS:
% in  = 2D mrs FIDA style data structre
%
%
% OUTPUTS:
% out = A frwquency slice of the 2D MRS data set stored in 1D FIDA data
%       structure
%

function [out] = op_freqSlice_2D(in, ppm1_val, ppm2_range);

% get the label for the metabolite I am fitting
switch ppm1_val
    case 2.0
        metabLab = 'Naa';
    case 3.0
        metabLab = 'Cr';
    case 3.2
        metabLab = 'Cho';
    case 3.5
        metabLab = 'ml';
end % end switch

        

% it in put data is a stucture then sotre in a cell
% this means when I pass a cell with multiple data set the function still
% works
if isstruct(in)   
    in{1,1} = in;
end

% get index for slice
[ d, ix_ppm1 ] = min(abs(in{1,1}.ppm1-ppm1_val));



% create a cell strucre to store 1D frequence slices
freqSlice = cell(numel(in), 1);

for i=1:numel(in)
    % data and info for 1D slice
    ppm = in{i,1}.ppm2;
    t = in{i,1}.t2;
    specs = in{i,1}.f2f1(:, ix_ppm1);
    sz = size(specs);
    
    fids=fftshift(ifft(specs,[],1),1);
    
    
    % FILL DATA STRUCTURE:
    % data info to modify for 1D FIDA style 
    freqSlice{i,1}.ppm = ppm;
    freqSlice{i,1}.t = t;
    
    % I need to FFT this to get the fid
    freqSlice{i,1}.fids = fids;
    
    freqSlice{i,1}.specs = specs;
    freqSlice{i,1}.sz = sz;
    
    % data info to copy directly from the 2D FIDA style 
    freqSlice{i,1}.averages = in{i,1}.averages;
    freqSlice{i,1}.Bo = in{i,1}.Bo;
    freqSlice{i,1}.date = in{i,1}.date;
    freqSlice{i,1}.dwelltime = in{i,1}.dwelltime;
    freqSlice{i,1}.pointsToLeftshift = in{i,1}.pointsToLeftshift;
    freqSlice{i,1}.rawAverages = in{i,1}.rawAverages;
    freqSlice{i,1}.rawSubspecs = in{i,1}.rawSubspecs;
    freqSlice{i,1}.seq = in{i,1}.seq;
    freqSlice{i,1}.spectralwidth = in{i,1}.spectralwidth;
    freqSlice{i,1}.subspecs = in{i,1}.subspecs;
    freqSlice{i,1}.te = in{i,1}.te;
    freqSlice{i,1}.tr = in{i,1}.tr;
    freqSlice{i,1}.txfrq = in{i,1}.txfrq;
    freqSlice{i,1}.watersupp = in{i,1}.watersupp;
    if isfield(in{i,1},'trm_fct')
        freqSlice{i,1}.trm_fct = in{i,1}.trm_fct;
    end
    freqSlice{i,1}.flags.inc = in{i,1}.inc;
    freqSlice{i,1}.flags = in{i,1}.flags;
    freqSlice{i,1}.dims = in{i,1}.dims;
    
    freqSlice{i,1}.metabLab = metabLab;  % label for which diagonal peak this is an extraction for 

    out = freqSlice;
    
    
    
    
    
    
end


end








