% Chris W: here I want to write a script that takes in my processsed 2D
% data and calculates Volume intergrals

% USAGE: 

% DESCRIPTION: 
% Input data would have passed through runpressproc_cw_fast.m and
% cosyPipelineTerra.m. This script will then take in calculate volume integrals and plot the the voume integral plots 
% The 2D matrix to pass from cosyPipelineTerra.m is Sbfiltspecs       
%           
%
% INPUTS:
% Spectral(Hz)2D cosy matrix    = FIDA_data_f2f1 - 2D cosy spectrum
% processed from raw data  -- zero padded  this is  Sbfiltspecs
%
% PPM2       = direct frequencey dimension -- zero padded
%
% PPM1       = indirect frequencey dimension -- zero padded
%
% PPM2Lim = range of ppm2 values to integrate over
%
% PPM1Lim = range of ppm1 values to integrate over

function [out] = io_volInt_2D(cosy_matrix_zp, ppm2_zp, ppm1_zp)
%% Define volume integrals
% diagonal peaks
vol_b.diag = [];  % structure to store the diagonal peaks 

%Structure vol_b.metabolite = [ ppm2 range ; ppm1 range]
% ---
vol_b.diag.lac_d = [1.255 1.335 ; 1.125 1.5];
% ---
vol_b.diag.naa_d =  [1.915 2.065 ; 1.82 2.2];   
%vol_b.diag.naa_d =   [1.915 2.065 ; 3.150 3.7]; % test
% ---
vol_b.diag.Cr_d = [2.95 3.065 ; 2.842 3.24];
% ---
vol_b.diag.Cho_d = [3.1 3.225 ; 3.0 3.35];
% ---
vol_b.diag.ml_d = [3.45 3.55 ; 3.30 3.75];  % not sure if this one is correct


% cross peaks
vol_b.cross = []; % structure to store the diagonal peaks -- _b is short for 'box'

%Structure vol_b.metabolite = [ ppm2 range ; ppm1 range]
% ---
vol_b.cross.lac_c_ab = [1.258 1.332 ; 3.85 4.30]; % ab = above
vol_b.cross.lac_c_bel = [4.025 4.14 ; 1.15 1.47]; % bel = below 
% ---
vol_b.cross.naa_c_ab = [2.4 2.53 ; 4.17 4.53];
vol_b.cross.naa_c_bel = [4.31 4.41; 2.23 2.9];
% ---
vol_b.cross.Glu_ab =  [2.018 2.13; 2.21 2.5];% Glu is the glutamate cross peak that does not fully overlap with gln in vivo 
vol_b.cross.Glu_bel = [2.275 2.37 ; 1.85 2.17]; 
% ---
vol_b.cross.Glux_ab = [2.025 2.15 ; 3.572 3.91];  % Glux is the glutamate cross peak that overlaps with the Gln signal in vivo 
vol_b.cross.Glux_bel = [3.69 3.76 ; 1.89 2.25];
% ---
vol_b.cross.mlCho_ab = [3.45 3.545 ; 3.87 4.21];
vol_b.cross.mlCho_bel = [3.999 4.075 ; 3.35 3.67];

%% Creat a structure to store voume integrals
Vol_int = [];
Vol_int.diagonal = [];
Vol_int.cross = [];

%% Get a 2D region of noise

ppm1_noise_min = 1;
ppm1_noise_max = 1.5;

ppm2_noise_min = -2;
ppm2_noise_max = -1;

noisewindow = cosy_matrix_zp(ppm1_zp>ppm1_noise_min & ppm1_zp<ppm1_noise_max, ppm2_zp>ppm2_noise_min & ppm2_zp<ppm2_noise_max);
abs_noisewindow = abs(noisewindow); % absolute noise signal 

mean_noise = mean(abs_noisewindow(:)); % means noise
sd_noise = std(abs_noisewindow(:)); % mean standard deviation 

nosie_thresh = mean_noise+(3*sd_noise);

Vol_int.noise_thresh = nosie_thresh;
Vol_int.noise_mean = mean_noise;
Vol_int.nosie_sd = sd_noise;

% plot noise region
hold on 
ppm2 = [ppm2_noise_min, ppm2_noise_max, ppm2_noise_max, ppm2_noise_min, ppm2_noise_min];
ppm1 = [ppm1_noise_min, ppm1_noise_min, ppm1_noise_max, ppm1_noise_max, ppm1_noise_min];
plot(ppm2, ppm1, 'b-', 'LineWidth', 2);

clear ppm1 ppm2 

%% plot volume boxes on 2D cosy spectra
% diagonal peak

fn_d = fieldnames(vol_b.diag);
for i=1:numel(fn_d)
    ppm = vol_b.diag.(fn_d{i});
    
    ppm2_min = ppm(1,1);
    ppm2_max = ppm(1,2);
    
    ppm1_min = ppm(2,1);
    ppm1_max = ppm(2,2);
    
    hold on 
    ppm2 = [ppm2_min, ppm2_max, ppm2_max, ppm2_min, ppm2_min];
    ppm1 = [ppm1_min, ppm1_min, ppm1_max, ppm1_max, ppm1_min];
    plot(ppm2, ppm1, 'b-', 'LineWidth', 2);
    
    % For cross peakscalculate volume integral of 2D region of signal in each for loop 
    % iteration 
    sigwindow_d = cosy_matrix_zp(ppm1_zp>ppm1_min & ppm1_zp<ppm1_max, ppm2_zp>ppm2_min & ppm2_zp<ppm2_max); 
    abs_sigwindow_d = abs(sigwindow_d); % absoulute signal 
    
    % use threshold calculated above to avoid noise contribution from absolute
    % volume integral calculation
    mask_thresh = abs_sigwindow_d < nosie_thresh;   
    abs_sigwindow_thresh = abs_sigwindow_d(~mask_thresh);
    vol_int_d = sum(abs_sigwindow_thresh);
    
    % Also calculate the signal of the most intense data point and SNR
    MaxSig_d = max(abs_sigwindow_thresh);
    
    SNR_d = MaxSig_d/Vol_int.nosie_sd; % calc SNR
        
    % store in structure
    Vol_int.diagonal.(fn_d{i}).VolInt = vol_int_d;    % volume integral
    Vol_int.diagonal.(fn_d{i}).MaxSig = MaxSig_d; % singal of most intense peak in volume integral
    Vol_int.diagonal.(fn_d{i}).SNR = SNR_d;     % SNR
    
end

clear ppm2_min ppm2_max ppm1_min ppm1_max

% cross peaks
fn_c = fieldnames(vol_b.cross);

for i=1:numel(fn_c)
    ppm_c = vol_b.cross.(fn_c{i}); 
    
    ppm2_min = ppm_c(1,1);
    ppm2_max = ppm_c(1,2);
    
    ppm1_min = ppm_c(2,1);
    ppm1_max = ppm_c(2,2);
    
    hold on 
    ppm2 = [ppm2_min, ppm2_max, ppm2_max, ppm2_min, ppm2_min];
    ppm1 = [ppm1_min, ppm1_min, ppm1_max, ppm1_max, ppm1_min];
    plot(ppm2, ppm1, 'b-', 'LineWidth', 2);
    
    % For cross peaks calculate volume integral of 2D region of signal in each for loop 
    % iteration 
    sigwindow_c = cosy_matrix_zp(ppm1_zp>ppm1_min & ppm1_zp<ppm1_max, ppm2_zp>ppm2_min & ppm2_zp<ppm2_max); 
    abs_sigwindow_c = abs(sigwindow_c); % absoulute signal 
    
    % use threshold calculated above to avoid noise contribution from absolute
    % volume integral calculation
    mask_thresh = abs_sigwindow_c < nosie_thresh;   
    abs_sigwindow_thresh = abs_sigwindow_c(~mask_thresh);
    vol_int_c = sum(abs_sigwindow_thresh);
    
    
    % Also calculate the signal of the most intense data point and SNR
    MaxSig_c = max(abs_sigwindow_thresh);
    SNR_c = MaxSig_c/Vol_int.nosie_sd;  % SNR
    
    %% store in structure
    Vol_int.cross.(fn_c{i}).VolInt = vol_int_c; % Volume integral
    Vol_int.cross.(fn_c{i}).MaxSig = MaxSig_c; % singal of most intense peak in volume integral
    Vol_int.cross.(fn_c{i}).SNR = SNR_c;   % SNR
end
clear ppm2_min ppm2_max ppm1_min ppm1_max 

out = Vol_int;
return 

z

