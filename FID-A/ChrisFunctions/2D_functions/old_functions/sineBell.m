function [Sbfiltspecs,Sbfiltfid_st,t1,t2] = sineBell(in_2d, beta_t1, inc, Nrep, beta_t2, BW, vec)
%SINEBELL Multiplies fid by a sine bell filter
%   This function is mainly for the out put of my 2D extracting function 
%   e.g(extract_cosy_FIDA_cw.m & extract_JP_FIDA_cw.m)

 
    %sz = size(t) 
    
    % dk ;     The windom funtion to for filtering the fid
    % beta;    when beta = 0; sine bell & when beta = 1; cosine-bell
    % tmax;    total time of aquisition in the time dimension (either direct or  indirect)  
    % 2dData;  input - unfiltered fid
    % inc;     t1 increment in ms 
    % Nrep;    number of repetitions in the indirect dimension 
    % vec;     Number of smapleing points in t2 dim
    
    inc = inc*10^-3;   % t1 inc is now is s
    t1max = inc*Nrep-inc;
    t1=[0:inc:t1max];
    
    t2dwt=1/BW;  % t2 dwell time
    t2max=t2dwt*vec-t2dwt; 
    t2=[0:t2dwt:t2max];
    
    
    % relivant equation from:
    % 'https://www.lifesci.sussex.ac.uk/nmr/docs/UserManual.pdf' - page 178
    % fid_sb = sin(beta+[pi-beta]*.t/tmax)
    dk_t1=sin(beta_t1+((pi-beta_t1).*t1/t1max)); % for indirect dimension
    dk_t2=sin(beta_t2+((pi-beta_t2).*t2/t2max));
    
    
    % Multiple the dk_t1 windowfunction along the t1 axis   
        for i=1:vec
            in_2d(i,:)=in_2d(i,:).*dk_t1;
        end
        
    % Multiple the dk_t2 windowfunction along the t2 axis
    
       dk_t2=dk_t2.';  % transpose
        for i=1:Nrep
            in_2d(:,i)=in_2d(:,i).*dk_t2;
        end
    
    % zero fill both direct and indirect dimension - in future change move away from hard coding this 
   % zfMtrx=padarray(in_2d,[2048,448],0,'post'); % zero filled matrix   for alex Lins data
    zfMtrx=padarray(in_2d,[4096,448],0,'post'); % for 7T data or when I use 2048 points at 3T
    %zfMtrx=padarray(in_2d,[2080,448],0,'post');
    
    Sbfilt_tt=zfMtrx;    % tt = both dims = time       
    Sbfiltfid_st= fftshift(ifft(Sbfilt_tt,[],1),1); % st = dims = spec & time
    Sbfiltfid_ss= fftshift(ifft(Sbfiltfid_st,[],2),2);  % s = both dims = spec
    
    % out put 2 by 2 matrix - spectrum 
    Sbfiltspecs = Sbfiltfid_ss; 
    
end
