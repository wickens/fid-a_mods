%this function extracts FID-A processed cosy data and stores it in an array
%This allows me to plot the 2D specrtum in matlab 


function [FIDA_data_t2t1,FIDA_data_f2t1,FIDA_data_f2f1,ppm2,ppm1] = extract_cosy_FIDA_cw(relivant_data,NRep,inc) 
% relivant_data - each individual FID-A TE is stored in FID-A file format
% NRep - number of repetitions/measurments 
% inc - increment in ms

FIDA_data_t2t1=ones(2048,NRep); % changed from 2048 to 1024 for alex L's braino data
%FIDA_data_t2t1=ones(1040,NRep);   % for 3T

for i=1:NRep
    FIDA_data_t2t1(:,i)=relivant_data{i,1}.out.fids; % for cosy remove out
end


FIDA_data_f2t1= fftshift(ifft(FIDA_data_t2t1,[],1),1);



FIDA_data_f2f1= fftshift(ifft(FIDA_data_f2t1,[],2),2);

ppm2=relivant_data{1,1}.out.ppm;   % for cosy remove out direct dimension 

f1_bw = 1/(inc*10^-3); % f1 band width
 
freq1=linspace(-(f1_bw/2), f1_bw/2, NRep);  % scale for the indirect f1 dimension 

% convert the f1 dimension in cosy to ppm by dividing by B0
Bo = relivant_data{1,1}.out.Bo; % in Tesla 
ppm1=(-freq1/(Bo*42.577))+4.75;% f/1H gyro magnetic ratio (f/42.577 MHz) - indirect dimension 




