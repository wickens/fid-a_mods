%chris w: this is my pipe line to take cosy 'relivant data' (processed from twix)

%% extract cosy data
    [FIDA_data_t2t1,FIDA_data_f2t1,FIDA_data_f2f1,ppm2,ppm1] = extract_cosy_FIDA_cw(relivant_data,64,0.8);  % was 58

%% reference direct dim2 dimension 
%ref = op_ppmref(relivant_data{1,1}.out,2.8,2.9,3.16);  % used 3.16 as it references the Cr peak to ~3.0 ppm for some reason
%ppm2 = (ref.ppm);

%% Extract first row indirect dimension into fida file format

[FID_A_ppm1]=cosyf1_into_FIDA(relivant_data, FIDA_data_t2t1,FIDA_data_f2f1,ppm1,0.8,64);  % was 58


%% refference spectrum
[FID_A_ppm1_ref,frqshift]=op_ppmref(FID_A_ppm1,1,3,2.15); % refference Naa dim1 peak

ppm1=FID_A_ppm1_ref.ppm;

%clear FID_A_ppm1 freq1 frqshift  
%% flip specc
%FID_A_ppm1_ref.specs=flipud(FID_A_ppm1_ref.specs);

%% plot

%op_plotspec(FID_A_ppm1_ref,-10,10)
 plot(FID_A_ppm1_ref.ppm, abs(FID_A_ppm1_ref.specs));

%% Now use this refferences ppm of axis when ploting the 2D matrix from extract_cosy_FIDA_cw.m out put 

% contor

figure(40)
contour(fliplr(ppm1),ppm2,abs(FIDA_data_f2f1),[0.15*10^-7: 0.15*10^-8: 1.5*10^-7]);
colormap('hot')
xlim([1 5.4]);
ylim([1 5.4]);
zlim([0 8.5*10^-7]);
title('NO Zero filling or filtering')
%caxis([0 100*10^-8]);

xlabel('ppm1');
ylabel('ppm2');                                                                                                                                             

%% sb filter & zero fill 

[Sbfiltspecs,Sbfiltfid_st,t1,t2] = sineBell(FIDA_data_t2t1, 0, 0.4, 64, 0.3, 2000, 1040);  % was 58   % change BW back to 2000 Hz

%% get new ppm values after zerofilling 
% re-calculate ppm vector for indirect dimension - ppm1
FID_A_ppm1_zp=op_zeropad(FID_A_ppm1_ref,8);
%%
% re-calculate ppm vector for direct dimension - ppm2
FID_A_ppm2_zp=op_zeropad(relivant_data{1,1}.out,3);
ppm2_zp=FID_A_ppm2_zp.ppm;

%% Shift ppm1
ppm1_zp=FID_A_ppm1_zp.ppm;
ppm1_zp=ppm1_zp+0.15;   % this refferencees the Cr ppm1 signal to 3 ppm 
%% plot filter + zero filled data
%%
Sbfiltspecs=Sbfiltspecs.';  % IMPORTANT: NB this is so ppm1 is on vertical axis
figure(300)
contour(ppm2_zp,fliplr(ppm1_zp),abs(Sbfiltspecs),[0.03*10^-8: 0.04*10^-9: 0.4*10^-8]);    % 0.1*10^-9
%contour(ppm2_zp,fliplr(ppm1_zp),abs(Sbfiltspecs),[0.004*10^-8: 0.04*10^-9: 0.30*10^-8]);
colormap('hot')
xlim([0.5 5.5]);  
ylim([0.5 5.5]);

xlim([-5 5.5]);  
ylim([-5 5.5]);

%zlim([0 1.5*10^-8]);
%title('3T l-COSY of GE braino phantom (Zero filling + SB filtering) 20130703'); 
title('3T phantom al-last-cosy with (Zero filling + SB filtering) , vec = 1024, BW = 2000 Hz');
%caxis([0 100*10^-8]);

xlabel('ppm2');
ylabel('ppm1');
%view(-180,90); 
view(-180,-90);   %% For same orientation as Verma

%%
%{
figure(300)
surf(fliplr(ppm1_zp),ppm2_zp,abs(Sbfiltspecs));
colormap('hot')

xlim([0 4.7]);
ylim([0 4]);
zlim([0 4*10^-9]);
caxis([-5*10^-9 10*10^-9]);
xlabel('ppm1');
ylabel('ppm2');

%}


