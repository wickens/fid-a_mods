% op_addNoise.m
% Chris Wickens, The University of Cambridge 2018.
% 
% USAGE:
% [P_st]=sim_dynamicFlow_1spin(in,t,S, S_st, P,V_in);
% 
% DESCRIPTION:
% Take in simulated data of a single spin (with const amplitude) and adds 
% dynamic flow inforamtion accroding to P_st = P*(S_st/S)*(1-exp((V_in*t/P)));
% the amplitude of the simulation changes with time like in 13C infusion. 
% 
% INPUTS:
% in      = Input data in matlab structure format.
% t       = infusion time
% S       = natural abundance concentration of substrate 
% S_st    = 13C labeled concentration of substrate (such that [S_st][S]= fractional enrichment)
% P       = natural abundance concentration of product 
% V_in    = rate constant (such that V_in/[P] is the rate)

%
% OUTPUTS:
% P_st        = 13C labeled concentration of product 
% 



function  [out, profile] = sim_dynamicFlow_1spin(in,t,S, S_st, P,V_in)
% definition the fucntion that describes the dynamic labeling of the Glu5 peak
% with time
    %t = [1:1:544];
    %S = 1;
    %S_st = 2;
    %P = 10;
    %V_in = -0.1;
    
    %P_st = ones(1,544) 
    P_st = P*(S_st/S)*(1-exp((V_in*t/P)));
    
    plot(t,P_st)
    
    % apply - the dynamic funciton to my fid and spec data -- is that
    % correct to do?
    
    in = op_comDataStruc(in); % convert data into single FIDA struct
    
    in.fids = in.fids*diag(P_st);    % diagonalise P_st for element wise multiplication 
    in.specs = in.specs*diag(P_st);  % diagonalise P_st for element wise multiplication
    
    in = op_comDataStruc2cell(in); % convert data into cell
    
    out = in;
    % IMPORTANT: if the initial simulated peak has a amp of 1 profile = P_st;  
    % if not the profile should be P_st*Amp - this code does not do this
    % automatically 
    profile = P_st; 
end 