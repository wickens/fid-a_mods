% Chris Wickens, The University of Cambridge 2015.    
% 
% sim_onepulse13C_TC_fast.m
%
% USAGE:
% [out] = sim_onepulse13C_timeCourse(n,sw,Bfield,linewidth,sys, centreFreq, NS, SD, TR, TRdmy)
% 
% DESCRIPTION:
% This function simulates a pulse-acquire in infusion time course (TC) experiment with an ideal
% (instantaneous) excitation pulse and an assumed lorentzian lineshape.  
% The function calls the function 'sim_Hamiltonian' which produces the free
% evolution Hamiltonian for the specified number of spins, J and shifts. To
% make it run faster I just simulate the system once and replicate it to
% form the base signlas for the time courese. The dynamic profile is also
% added
% 
% INPUTS:
% n         = number of points in fid/spectrum
% sw        = desired spectral width in [Hz]
% Bfield    = main magnetic field strength in [T]
% linewidth = linewidth in [Hz]
% sys       = spin system definition structure
% centreFreq = Where to put my center frequency  -- % chris W 2402221 
% NS          = number of scans (aquistions) 
% TR          = repitition time 
% TRdmy =  the number of dummy scans to run  -- This is to get the infusion time right
% 
%
% OUTPUTS:
% out       = simulated spectrum, in FID-A structure format, using pulse-acquire 
%             sequence.

function [out] = sim_onepulse13C_TC_fast(n,sw,Bfield,linewidth,sys, centreFreq, NS, TR, DynamicProf, TRdmy, ang, T1)
if nargin < 12
    T1 = 0;
    if nargin < 11
        ang = 90;   
    end
end
TimeC_d = cell(NS, 1);

% create a profile of amplitudes to resuble to labl=eling of metabilites
% during the time course 

% First make a time course where the spin amplitue stay constant  
for i=1:NS
    if i == 1
        %TimeC_d{i,1} = sim_onepulse13C(n,sw,Bfield,linewidth,sys, centreFreq);
        TimeC_d{i,1} = sim_onepulse13C_T1Rlx(n,sw,Bfield,linewidth,sys, centreFreq, ang, T1, TR);
    else
        TimeC_d{i,1} = TimeC_d{1,1}; 
    end
    
    % add noise
    %TimeC_d{i,1} = op_addNoise(TimeC_d{i,1},SD);
end 

%% now add in the dynamic profile that was simulated in CWAVE
x = DynamicProf.time;
y = DynamicProf.conc.';
TimeC_d = op_comDataStruc(TimeC_d);

y = repmat(y,[TimeC_d.sz(1),1]);

TimeC_d.fids = TimeC_d.fids.*y;
TimeC_d.specs = TimeC_d.specs.*y;

out = TimeC_d;




