figure; plot(glu_d.time,glu_d.conc,':','LineWidth', 2)  % glu -- ground truth
hold on
plot(glu_d.time,glu_d.conc*(1-f_t1Relax(1,TR,T1_Glu,ang)),':','LineWidth', 2)  % glu -- ground truth
%plot(glu_d.time,glu_d.conc*f_t1Relax(1,TR,T1_Glu,ang),':','LineWidth', 2)  % glu -- ground truth
%plot(glu_d.time,glu_d.conc*(sind(ang)),':','LineWidth', 2)  % glu -- ground truth
plot(glnAsp_d.time, amp_Arr.GluC5,'--', 'LineWidth', 2)
%plot(glu_d.time,glu_d.conc*0.42,':','LineWidth', 2)  % glu -- ground truth


legend('GT No cor (GluC5)', 'GT Cor (GluC5)', 'AMARES fit (glu)', ...
    'Location','NorthEastOutside')

%legend('GT No cor (GluC5)', 'GT Cor (GluC5)', 'AMARES fit (glu)', ...
 %   'GT Cor 2nd (GluC5)','Location','NorthEastOutside')
 
 
 
 
%% when flip angle = 48 but T1 = 0
figure;
plot(glu_d.time,glu_d.conc,':','LineWidth', 2)  % glu -- ground truth
hold on
%plot(glu_d.time,glu_d.conc*(f_t1Relax(1,TR,T1_Glu,ang)),'--','LineWidth', 2)  % glu -- ground truth
plot(glnAsp_d.time, amp_Arr.GluC5*(1/sind(ang)),'--', 'LineWidth', 2)
legend('GT No cor (GluC5)', 'GT Cor (GluC5)', 'AMARES fit (glu)', ...
    'Location','NorthEastOutside')

%% when flip angle = 48 but T1 = 16

figure;
plot(glu_d.time,glu_d.conc,':','LineWidth', 2)  % glu -- ground truth
hold on
plot(glnAsp_d.time, amp_Arr.GluC5,'--', 'LineWidth', 2)
%plot(glu_d.time,glu_d.conc*(f_t1Relax(1,TR,T1_Glu,ang)),'--','LineWidth', 2)  % glu -- ground truth
plot(glnAsp_d.time, amp_Arr.GluC5*(1/sind(ang)),'--', 'LineWidth', 2)
plot(glnAsp_d.time, amp_Arr.GluC5*(1/f_t1Relax(1,TR,T1_Glu,ang))*(1/sind(ang)),'--', 'LineWidth', 2)
legend('GT No cor (GluC5)', 'amp_Arr.GluC5', 'amp_Arr.GluC5*(1/sind(ang))', ...
    'Location','NorthEastOutside')



