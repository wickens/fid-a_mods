% The Universtiy of Cambridge 200421
%
% USAGE:
% [out] = sim_onepulse13C_timeCourse(n,sw,Bfield,linewidth,sys, centreFreq, NS, SD, TR, TRdmy)
% 
% DESCRIPTION:
% Calulates the T1 relaxation factor fiven a flip angle and a T1 relaxation constant  
%
% INPUTS:
% Mo     = equilibrium magnetisation along z 
% 
%
% OUTPUTS:
% Mz_factr     = relaxation factor to be applied to simulated data 



function [Mz_factr] = f_t1Relax(Mo,TR,T1,ang)

Mz_factr = Mo*(1-exp(-TR/T1))/(1-cosd(ang)*exp(-TR/T1));
