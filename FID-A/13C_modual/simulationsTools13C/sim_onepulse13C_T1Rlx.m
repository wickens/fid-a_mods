% chris W: 2021 -- adapted to simulate 13C spectra -- 
% new name: sim_onepulse13C.m
%
%
%sim_onepulse.m
%Robin Simpson and Jamie Near, 2014.
%
% USAGE:
% out = sim_onepulse13C_T1Rlx(n,sw,Bfield,linewidth,sys, centreFreq, ang, T1, TR)
% 
% DESCRIPTION:
% This function simulates a pulse-acquire experiment with an ideal
% (instantaneous) excitation pulse (of angle 'ang') and an assumed lorentzian lineshape.  
% The function calls the function 'sim_Hamiltonian' which produces the free
% evolution Hamiltonian for the specified number of spins, J and shifts.
% This is different from sim_onepulse13C in that I can define the flip
% angle. The function also includes T1 relaxation. This code assumes that 
% steady state has been reach for the logitudinal magniiastion of
% the spin system. 
%
% 
% INPUTS:
% n         = number of points in fid/spectrum
% sw        = desired spectral width in [Hz]
% Bfield    = main magnetic field strength in [T]
% linewidth = linewidth in [Hz]
% sys       = spin system definition structure
% centreFreq = Where to put my center frequency  -- % chris W 2402221 
%  ang     = flip angle of excitation pulse
%  T1      = T1 relaxation time constant 
%  TR      = repetition time of the experiment
%
% OUTPUTS:
% out       = simulated spectrum, in FID-A structure format, using pulse-acquire 
%             sequence.

function out = sim_onepulse13C_T1Rlx(n,sw,Bfield,linewidth,sys, centreFreq, ang, T1, TR)

%Set water to centre
% centreFreq=4.65;
%centreFreq = 120; % chris W think what this should be 
%centreFreq = 182;
%centreFreq = 0;


for k=1:length(sys)
    sys(k).shifts=sys(k).shifts-centreFreq;
end

% calc scale factor that accounts for T1 relaxation
for i=1:length(sys)
    sys(i).scaleFactor = f_t1Relax(sys(i).scaleFactor,TR,T1,ang);
end
%Calculate Hamiltonian matrices and starting density matrix.
[H,d]=sim_Hamiltonian13C(sys,Bfield);   % chris W 230321: for 13C implementation


%BEGIN PULSE SEQUENCE************
%d=sim_excite(d,H,'x');                            %EXCITE


d=sim_excite(d,H,'x',ang);                            %EXCITE
[out,dout]=sim_readout13C(d,H,n,sw,linewidth,90);  %Readout along y (90 degree phase);  % chris W 230321: for 13C implementation
%END PULSE SEQUENCE**************

% Add T1 relaxation

% out.fids = out.fids*Mz; 
% out.specs = out.specs*Mz; 

%Correct the ppm scale:
%out.ppm=out.ppm-(4.65-centreFreq);
%out.ppm=out.ppm-centreFreq; % chris W 230321: for 13C implementation 

%Fill in structure header fields:
out.seq='onepulse';
out.te=0;
out.sim='ideal';

%Additional fields for compatibility with FID-A processing tools.
out.sz=size(out.specs);
out.date=date;
out.dims.t=1;
out.dims.coils=0;
out.dims.averages=0;
out.dims.subSpecs=0;
out.dims.extras=0;
out.averages=1;
out.rawAverages=1;
out.subspecs=1;
out.rawSubspecs=1;
out.flags.writtentostruct=1;
out.flags.gotparams=1;
out.flags.leftshifted=0;
out.flags.filtered=0;
out.flags.zeropadded=0;
out.flags.freqcorrected=0;
out.flags.phasecorrected=0;
out.flags.averaged=1;
out.flags.addedrcvrs=1;
out.flags.subtracted=1;
out.flags.writtentotext=0;
out.flags.downsampled=0;
out.flags.isISIS=0;


   
end
