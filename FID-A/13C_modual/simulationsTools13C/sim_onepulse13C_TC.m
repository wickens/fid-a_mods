% Chris Wickens, The University of Cambridge 2015.    
% 
% sim_onepulse13C_TC.m
%
% USAGE:
% [out] = sim_onepulse13C_timeCourse(n,sw,Bfield,linewidth,sys, centreFreq, NS, SD, TR, TRdmy)
% 
% DESCRIPTION:
% This function simulates a pulse-acquire in infusion time course (TC) experiment with an ideal
% (instantaneous) excitation pulse and an assumed lorentzian lineshape.  
% The function calls the function 'sim_Hamiltonian' which produces the free
% evolution Hamiltonian for the specified number of spins, J and shifts.
% 
% INPUTS:
% n         = number of points in fid/spectrum
% sw        = desired spectral width in [Hz]
% Bfield    = main magnetic field strength in [T]
% linewidth = linewidth in [Hz]
% sys       = spin system definition structure
% centreFreq = Where to put my center frequency  -- % chris W 2402221 
% NS          = number of scans (aquistions) 
% SD         = standard diviatino of noise to add. 
% TR          = repitition time 
% TRdmy =  the number of dummy scans to run  -- This is to get the infusion time right
% 
%
% OUTPUTS:
% out       = simulated spectrum, in FID-A structure format, using pulse-acquire 
%             sequence.

function [out] = sim_onepulse13C_TC(n,sw,Bfield,linewidth,sys, centreFreq, NS, SD, TR, TRdmy)

TimeC_d = cell(NS, 1);

% create a profile of amplitudes to resuble to labl=eling of metabilites
% during the timme course -- ending at amplitude 1




for i=1:NS
    TimeC_d{i,1} = sim_onepulse13C(n,sw,Bfield,linewidth,sys, centreFreq);
    
    % add noise
    TimeC_d{i,1} = op_addNoise(TimeC_d{i,1},SD);
end 


out = TimeC_d;




