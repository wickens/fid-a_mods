% Chris W The university of cambridge 040521:
%
% sim_213GlcInf.m
%
% 
% USAGE:
% [out, sysGlcC2_SF1_old, sysGlcC2_SF2_old]=sim_213GlcInf(glu_d, glnAsp_d, Glc_d, LW_Glu,LW_Gln,LW_Asp,LW_Glc, ...
%    T1_Glu, T1_Gln, T1_Glc, T1NOE_CorGlu, T1NOE_CorGlnAsp, T1NOE_CorGlc, ...
%    TR, sw, Bfield, n, centerFreq, ang)
% 
% DESCRIPTION:
% Simulate the [2-13C]Glc infusion time course. 
% 
% INPUTS:

%
% OUTPUTS:
%
%


function [out, sysGlcC2_SF1_old, sysGlcC2_SF2_old]=sim_213GlcInf(glu_d, glnAsp_d, Glc_d, LW_Glu,LW_Gln,LW_Asp,LW_Glc, ...
    T1_Glu, T1_Gln, T1_Glc, T1NOE_CorGlu, T1NOE_CorGlnAsp, T1NOE_CorGlc, ...
    TR, sw, Bfield, n, centerFreq, ang)

%% genearl simulation options
TRdmy = 0;
NS = length(glu_d.conc);
%TR = 8; % secs
%n = 4096; % pnts
%sw = 5500; % Hz
%Bfield = 3.0; % Tesla
%centerFreq = 153;  % excitation frequency of pulse
%ang = 48; % flip angle

%offset = -centerFreq; %-153;


%% simulate spectra for Glc, Glu, aspGln
%% GluC5
% load
load GluC5_13C.mat  % output = sysGluC5
%LW_Glu = 3.0; % Hz

DynamicProf_glu = glu_d;

% add combine T1 NOE factor obtained from HC3 in vivo data
%T1NOE_CorGlu = 1.577;
sysGluC5.scaleFactor = sysGluC5.scaleFactor*(1/T1NOE_CorGlu);

%ang = 48; %90;%48; % degrees
%T1_Glu = 0;%16; %0;%16; % secs
[Glu_TC] = sim_onepulse13C_TC_fast(n,sw,Bfield,LW_Glu,sysGluC5, centerFreq, NS, TR, DynamicProf_glu, TRdmy, ang, T1_Glu);
%Glu_TC = op_comDataStruc2cell(Glu_TC);

%% GlnC5Aspc4 
load GlnC5_13C.mat
load AspC4_13C.mat

% make Gln and Asp in the ratio 3/1 so they comnide to give apm of 1 (before adding metabilic profile)
sysGlnC5.scaleFactor = sysGlnC5.scaleFactor*(2/3); 
sysAspC4.scaleFactor = sysAspC4.scaleFactor*(1/3);

% add combine T1 NOE factor obtained from HC3 in vivo data
%T1NOE_CorGlnAsp = 1.577;
sysGlnC5.scaleFactor = sysGlnC5.scaleFactor*(1/T1NOE_CorGlnAsp); 
sysAspC4.scaleFactor = sysAspC4.scaleFactor*(1/T1NOE_CorGlnAsp);

%LW_Gln = 4.5; % change this to what I saw in vivo
DynamicProf_glnAsp = glnAsp_d;
%T1_Gln = 0;%16;
%pk_gln = PK_3T_Gln5Sim();

[Gln_TC] = sim_onepulse13C_TC_fast(n,sw,Bfield,LW_Gln,sysGlnC5, centerFreq, NS, TR, DynamicProf_glnAsp, TRdmy, ang, T1_Gln);

% asp
T1_Asp = T1_Gln;
%LW_Asp = 4.5;
[Asp_TC] = sim_onepulse13C_TC_fast(n,sw,Bfield,LW_Asp,sysAspC4, centerFreq, NS, TR, DynamicProf_glnAsp, TRdmy, ang, T1_Asp);

%% GlcC2
load GlcC2_13C.mat

sysGlcC2_SF1_old = sysGlcC2(1).scaleFactor;
sysGlcC2_SF2_old = sysGlcC2(2).scaleFactor;
% add combine T1 NOE factor obtained from HC3 in vivo data
%T1NOE_CorGlc = 0.65;
sysGlcC2(1).scaleFactor = sysGlcC2(1).scaleFactor*(1/T1NOE_CorGlc);
sysGlcC2(2).scaleFactor = sysGlcC2(2).scaleFactor*(1/T1NOE_CorGlc);


%LW_Glc = 3.0; % Change this to what I saw in vivo
DynamicProf_Glc = Glc_d;
%T1_Glc = 0;%1.9;%1.9; % secs
[Glc_TC] = sim_onepulse13C_TC_fast(n,sw,Bfield,LW_Glc, sysGlcC2, centerFreq, NS, TR, DynamicProf_Glc, TRdmy,ang, T1_Glc);


%% Combine all simulations into the same time course 

fullMetab_TC = op_addScans(Glu_TC, Gln_TC);
fullMetab_TC = op_addScans(fullMetab_TC, Asp_TC);
fullMetab_TC = op_addScans(fullMetab_TC, Glc_TC);

out = fullMetab_TC;





