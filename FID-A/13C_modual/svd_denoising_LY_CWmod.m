function [ out ] = svd_denoising_LY( data, method )
% Perform SVD de-noising of time-series data
%
% Input: 
% data   : Spectra in FID-A data structure. Note all timeseries data must
%          be in 1 array 
% method : option to filter either by number of metabolites or by
%          magnitude of contribution

numMetabolites = 10; % Hard coded for 31P MRS spectra
threshold = 1e-5; % Arbitrarily chosen. Should leave in very small signal contributions... probably at the cost of some noise

[U,S,V] = svd(data.specs);

Snew = S;
Snoise = S;

if method == 1
    Snew((numMetabolites+1):end, (numMetabolites+1):end) = 0;
    Snoise(1:numMetabolites, 1:numMetabolites) = 0;
elseif method == 2
    Snew( Snew < threshold ) = 0;
    Snoise(Snoise < threshold ) = 0;
end

specsDenoised = U*Snew*V';
noise = U*Snoise*V';
    
% % Note that this is a linear operation so can be done in either frequency
% % or time-domain with exactly the same results... 
[U_fid,S_fid,V_fid] = svd(data.fids);
 
Snew = S_fid; 
Snew((numMetabolites+1):end, (numMetabolites+1):end) = 0; 
 
fidsDenoised = U_fid*Snew*V_fid';
 
specsDenoised = fftshift(ifft(fidsDenoised,[],data.dims.t),data.dims.t);

out = data; 
out.specs = specsDenoised; 
out.noise = noise;
% chris W:
out.fids = fidsDenoised;



