%Chris W: code to calculate the SNR of peaks in my 13C spectra 
% I am not going to change the variable names but this code is not now 
% designed to just look NAA peaks 

% op_getSNR.m
% Jamie Near, McGill University 2014.
% 
% USAGE:
% [SNR]=op_getSNR_13C(in,NAAppmmin,NAAppmmax,noiseppmmin,noiseppmmax);
% 
% DESCRIPTION:
% Find the SNR of the NAA peak in a spectrum .
% 
% INPUTS:
% in             = input data in matlab structure format
% NAAppmmin      = min of frequncy range in which to search for NAA peak.
%                  (Optional.  Default = 1.8 ppm);
% NAAppmmax      = max of frequncy range in which to search for NAA peak.
%                  (Optional.  Default = 2.2 ppm);
% noiseppmmin    = min of frequency range in which to measure noise.
%                  (Optional.  Default = -2 ppm);
% noiseppmmax    = max of frequency range in which to measure noise.
%                  (Optional.  Default = 0 ppm);
%
% OUTPUTS:
% SNR            = Estimated SNR of the input spectrum.

function [SNR,signal,noisesd]=op_getSNR_13C(in,NAAppmmin,NAAppmmax,noiseppmmin,noiseppmmax);


if nargin<5
    noiseppmmax=0.8;      %origanal =0
    if nargin<4
        noiseppmmin=-0.2;  %origanal =-2
        if nargin<3
            NAAppmmax=2.2; %origanal =2.2               2.9
            if nargin<2
                NAAppmmin=1.7; %origanal =1.8           2.8
            end
        end
    end
end


%FIRST FIND THE NAA SIGNAL INTENSITY.  USE THE MAX PEAK HEIGHT OF THE 
%MAGNITUDE SPECTRUM INSIDE THE DESIRED SPECTRAL RANGE:
NAAwindow=in.specs(in.ppm>NAAppmmin & in.ppm<NAAppmmax);
ppmwindow=in.ppm(in.ppm>NAAppmmin & in.ppm<NAAppmmax);

maxNAA_index=find(abs(NAAwindow)==max(abs((NAAwindow))));
maxNAA=abs(NAAwindow(maxNAA_index))

%chris w: comment out all the ploting calls to stop the code crashing 
%infigure;                              
%plot(ppmwindow,abs(real(NAAwindow)));  

%chris w: comment out all the ploting calls to stop the code crashing 
%figure                
%plot(in.ppm,in.specs); 
%noiseppmmin=input('input lower ppm limit for noise: ');
%noiseppmmax=input('input upper ppm limit for noise: ');

%NOW FIND THE STANDARD DEVIATION OF THE NOISE:
noisewindow=in.specs(in.ppm>noiseppmmin & in.ppm<noiseppmmax);
ppmwindow2=in.ppm(in.ppm>noiseppmmin & in.ppm<noiseppmmax)';

P=polyfit(ppmwindow2,noisewindow,2);
noise=noisewindow-polyval(P,ppmwindow2);

%chris w: comment out all the ploting calls to stop the code crashing 
 %figure                                    
 %plot(ppmwindow2,noisewindow,...           
  %   ppmwindow2,polyval(P,ppmwindow2),...  
  %  ppmwindow2,noise);                    

signal=(maxNAA-mean(real(noisewindow))) %Removes DC offset

noisesd=std(real(noise))

%SNR=maxNAA/noisesd
SNR=signal/noisesd;