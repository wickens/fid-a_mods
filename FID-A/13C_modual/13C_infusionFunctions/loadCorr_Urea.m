% chris W : This function:
%    1) reads the urea load correction signal
%    2) allows you to phase the urea signal
%    3) gives a value for the SNR and the Urea signal integral 

function [out, SNR, fit, parsFit,residual] = loadCorr_Urea()

out = read_13C_dicom();

SpecTool(out{1,1},0.05,-2,7);
ph0=input('input 0 order phase correction: ');
ph1=input('input 1st order phase correction: ');

out{1,1} = op_addphase(out{1,1},ph0,ph1);

SNR = op_getSNR(out{1,1},-10,10,-25,50);
[fit,parsFit,residual]=op_peakFit_13C(out{1,1},-10,10);




