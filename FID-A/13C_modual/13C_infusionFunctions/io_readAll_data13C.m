% Chris W :13C - This funtion will read all dcm and nii from 13C infusion
% scan and store them in separte places - it will remove the phenix scans

% output = is a structure containing two fields out.DICOMS_str & out.nii_str

function [out] = io_readAll_data13C()

%% read all the files into a structure 
direct = dir;

%% Create a stucture with all the names of the files 

numDirect = numel(direct);
directName = strings([numDirect,1]); 

for i=1:numDirect
    directName(i,1) = direct(i,1).name;
end
clear i 

%% Put all dcm and nii files in there own folder
% .dcm files
% count how many dcm file there are 

numberOfdcm = 0;
NumberOfNii = 0;
for i = 1:numDirect
    if contains(directName(i),'dcm')
        numberOfdcm = numberOfdcm + 1;
    end
    
    if contains(directName(i),'.nii')
        NumberOfNii = NumberOfNii + 1;
    end
end
clear i

% set size of string array and fill the names
DICOMS_str = strings([numberOfdcm, 1]);
nii_str = strings([NumberOfNii, 1]);

DICOM_number = 0;
nii_number = 0;
for i = 1:numDirect
    if contains(directName(i),'.dcm')
         DICOM_number = DICOM_number + 1;
         DICOMS_str(DICOM_number,1) = directName(i,1);
    end
      
   if contains(directName(i),'.nii')
         nii_number = nii_number + 1;
         nii_str(nii_number,1) = directName(i,1);
    end

end

clear numDirect 

%% remove the phenix report from the dicoms '99'

IDX = strfind(DICOMS_str,'_0099_');
TF = cellfun('isempty', IDX);
DICOMS_str = DICOMS_str(TF);

out.DICOMS_str = DICOMS_str;
out.nii_str = nii_str;
clear i DICOM_number numberOfdcm nii_number NumberOfNii IDX TF



