% Chris W : this function will take a cell containing all the baselines and 
          % another cell containing all the infusionn data.
          
          % Chris W 13C 
          
          
          %First: It will align all the basline averages to the first
          % baseline average
          
          % Second: It will also align all the infusion spectra to the 
          % first baseline average
          
          
          function [aligned_13C] = io_align_13C_spectra(bl, inf)
          % bl = baseline data
          % inf = all infusion data
          
          % Align the baselines to the first baseline
          sz_bl = size(bl);
          sz_bl = sz_bl(1,1);
                 
          bl_AL = cell(sz_bl,1);  % bl_AL = baselines aligned
          for i=1:sz_bl  
              bl_AL{i,1}=op_alignScans(bl{i,1},bl{1,1},0.2046);  %-- old working
              %bl_AL{i,1} = op_alignScans_fd(bl{i,1},bl{1,1},38,42,0.2046);
          end
          
          
          % Align the insfusion to the first baseline
          sz_inf = size(inf);
          sz_inf = sz_inf(1,1);
          
          inf_AL = cell(sz_inf,1); % inf_AL = infusions aligned 
          for i=1:sz_inf
              inf_AL{i,1}=op_alignScans(inf{i,1},bl{1,1},0.2046); % -- old working 
              %inf_AL{i,1} = op_alignScans_fd(inf{i,1},bl{1,1},38,42,0.2046);
          end
          
          aligned_13C.bl_AL = bl_AL;
          aligned_13C.inf_AL = inf_AL;
          
          end