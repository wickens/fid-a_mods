% Chris W: this script is based on belindas CardiacMRS_wetPhaseSpec code

function [ phasedSpecArray, phaseArray, waterAmp ] = phase_13C(specObj)
% phase_13C Phases a spectra by its fat peak at 172.5 ppm. 
%Input is an unphased specObj, and it phases each of the spectrum
%individually. All fat peaks are up
?
?
signal = specObj;
phasedSpecArray = zeros(numel(signal.spectra), length(signal.spectra{1}));
phaseArray = zeros(numel(signal.spectra), 1);
for k = 1 : numel(signal.spectra)
    inst = k;
    
    tempSpec = signal.spectra{inst};
    if tempSpec(1, 1) ~= 0
        
        %% ignore this bit if not using ameres fit as initial guess
        n_voxel = 1;
        fittingOptions.pk.all = AMARES.priorKnowledge.PK_1H_Cardiac_bd_water_singlepeak();
        fittingOptions.beginTime = 0;
        fittingOptions.offset = -3.2;
        
        % fits the fat peak with the same pk file as a single peak water
        % but with offset set to -3.2 ppm i.e. the fat
        fit = AMARES.amares(signal, inst, n_voxel, fittingOptions.beginTime, fittingOptions.offset,  AMARES.priorKnowledge.PK_1H_Cardiac_bd_water_singlepeak(), 0, 'fixOffset', -3.2,'MaxIter', 500);
        %% phasing on fat lipit
        
        n_points = length(signal.spectra{k});
        [~, fatPoint] = min(abs(signal.ppmAxis + 3.2)); % fat peak at -3.2ppm relative to water
        fatRange = [fatPoint - 40: fatPoint + 40]; % reduce range if dealing with water
        spec = double(signal.spectra{k});
        [fatHeight, fatPeakIdx] = max(abs(spec(fatRange)));
        fatPeakIdx = fatPeakIdx + fatPoint - 40 - 1;
        
        a = find(abs(spec) < 0.6 * fatHeight);
        a_lower = a(max(find(a < fatPeakIdx)));
        a_upper = a(min(find(a > fatPeakIdx)));
        
        fun = @(phi) (real(spec(a_lower - 6 : a_lower)*exp(1i * phi)) - real(spec(a_upper : a_upper + 6) * exp(1i * phi)));
        
        options = optimoptions('lsqnonlin');
        options = optimset('MaxFunEvals',3000, 'TolFun', 1e-10 );
        phi_0 = mod(-1 * fit.Phases(1), 360)/360 * 2 * pi; % replace fit.phase(1) with initial guess if not using AMARES
        
        phase = lsqnonlin(fun, phi_0, 0, 2*pi, options);
        
        spec_temp = spec * exp(1i * phase);
        
        % check that water peak is up (optional)
        if real(spec_temp(fatPeakIdx)) < 0
            spec_phased = spec * exp(1i * pi);
        else
            spec_phased = spec_temp;
        end
        
        phasedSpecArray(k, :) = spec_phased;
        phaseArray(k) = phase;
        waterAmp(k) = fit.Amplitudes(1);
        
    else
        
        waterAmp(k) = 0;
    end
    
    % for debugging
%     figure(1000)
%     clf; hold on
%     plot(signal.ppmAxis, tempSpec)
%     plot(signal.ppmAxis, spec_phased)
%     legend('original', 'new')
%     title(strcat(string(k), ' ', string(phase)));
%     pause
end
end