% op_freqshift.m
% Chris Wickens, The University of Cambridge 2021.
% 
% USAGE:
% out=op_slidWindow(in,wndSz);
% 
% DESCRIPTION:
% Data will be averaged such that each spectra in the time course will 
% becomes the average of its neighbors. The tuning parameter here is window
% width - I.e the number of neighbouring that are averaged to produce each 
% each spectrum
% 
% 
% INPUTS:
% in     = FIDA data where each aquisition is in its own cell.
% wndSz = windo size (number of averages to combine)
%
% OUTPUTS:
% out    = sliding window averages data set.  




function out=op_slidWindow(in,wndSz);


% take data in cell format
totAvs = numel(in);

% Convert the dat into a single FIDA structure where the averages dimension
% is effectively each average in the infusion time course 
%

% prealocate memory to store sliding average data 
slidAvData = cell(totAvs-wndSz,1);

for i=1:(totAvs-wndSz)
    windAv = in(i:wndSz+(i-1),1);
    windAv = op_comDataStruc(windAv);
    %windAv = op_Average_block(windAv,numel(windAv));
    
    % make sure the averages dimension is seen in the dims field 
    windAv.dims.averages = 2;
    windAv = op_averaging(windAv);
    
    slidAvData{i,1} = windAv;
    clear windAv
end

out = slidAvData;







