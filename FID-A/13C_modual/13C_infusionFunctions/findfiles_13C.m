% Chris W : I want this function to read in all the .dcm files into a structure  

function out = findfiles_13C(directory,string,r)

if nargin < 1 
    out = [];
    return
end
if nargin < 2
    string = directory;
    directory = pwd; 
end
if nargin < 3
    r = 1; 
    string=['.*' string '.*'];
end
if numel(string) < 1
    error('Need to specify something to find');
end
out = getindir_13C(directory, string, r);

