% chris W 13C  - This funtion will use chris R SiemensCsaParse.m and
% SiemensCsaReadFid.m tools to find out the all the sequence info as including 
% names of sequences on the scanner (only for .dcm file)

% NB: use this function after using io_readAll_data13C()

% output = cell containing the sequence string for each sequence

function [out] = io_readSeqInfo_13C(in)

DICOMS_str = in.DICOMS_str;

numberOfdcm = length(DICOMS_str);

info = cell(numberOfdcm,1);
for i=1:numberOfdcm
name_char = char(DICOMS_str(i));
%Load Dicom Info using Chris Rogers' "SiemensCsaParse.m" function:
info{i,1}=SiemensCsaParse(name_char);

%Read in Dicom file using Chris Rogers' "SiemensCsaReadFid.m" function:
[fids,info{i,1}]=SiemensCsaReadFid(info{i,1},0);
end
 out = info;

clear numberOfdcm i
