% Chris Wickens The University of Cambridge 060521:
%
% calc_calcRelRMSE.m
%
% 
% USAGE:
% calc_calcRelRMSE(All_GT, amp_Arr_noise, amp_Arr_deoise);
% 
% DESCRIPTION:
% Calculates the RMSE of data fitted before and after denoising. 
% The caluates the relative RMSE. 
% 
% INPUTS:
% All_GT          = ground truth values of dynamic profile from Cwave 
%                 (To make the points evenly spaced the profile was fitted with many (~100) splines to prevent bias when denoising splines 
%                 (which uses much less less is pieces) try to find the Ground truth. 
% amp_Arr_noise   = dynamic profile calulated before denoising
% amp_Arr_deoise  = dynamic profile calulated after denoising
% 
%
% OUTPUTS:
% out   = realtive RMSE for each metablite signal


function [out] = calc_calcRelRMSE(All_GT, amp_Arr_noise, amp_Arr_deoise);


% get the field names for the ground truths 
fn_GT = fieldnames(All_GT);

% loop through the ground truths meaolite time course to calculate relative
% RMSE between reconstucted time course and the ground truth for both
% noised and denoised version. Then calc relative RMSE between them.

for i=1:length(fn_GT)
    % calc the RMSE for the noised case
    RMSE_noise.(fn_GT{i}) = sqrt(mean((amp_Arr_noise.(fn_GT{i}) - All_GT.(fn_GT{i})).^2)); 
    
    % calc the RMSE for the de noised case
    RMSE_DeNoise.(fn_GT{i}) = sqrt(mean((amp_Arr_deoise.(fn_GT{i}) - All_GT.(fn_GT{i})).^2));    
    
    % calc the relivive RMSE's
    rel_RMSE.(fn_GT{i}) = RMSE_DeNoise.(fn_GT{i})/RMSE_noise.(fn_GT{i});
end


out = rel_RMSE;
