% op_filtLorenzGaus.m
% Chris Wickens, The University of cambridge 2014.
% 
% USAGE:
% [out,lor]=op_filtLorenzGaus(in,lb_lorenz, lb_Gaus)
% 
% DESCRIPTION:
% Perform Lorentz-Gaussian line broadening by multiplying the time domain 
% signal by the sub of a increasing exponential and a gaussian decasy.
% This converst lorentzian to a more narrow gaussian line shape (See R. Degraphs book 
% second edition  -- page 21).
% 
% INPUTS:
% in     = input data in matlab structure format.
% lb_lorenz    = Lorenzian line broadening factor in Hz .
% lb_Gaus  = Gaussian line broadening factor in Hz .
%
% OUTPUTS:
% out    = Output following alignment of averages.  
% lor    = Exponential (time domain) filter envelope that was applied.


function [out,lor]=op_filtLorenzGaus(in,lb_lorenz, lb_Gaus);

fids = in.fids;

% calc get t2 constants for lorenzian and gaussian
t2_lorenz = 1/(pi*lb_lorenz);
t2_Gauss = 1/(pi*lb_Gaus);

% make exponentialy increasing function -- if lb_lorenz is negative 
lor = exp(-in.t/t2_lorenz);
% figure
% plot(in.t,lor);
% title('')
% xlabel('Time (s)')
% ylabel('amp (a.u.)')

% make gaussian increasing function
gaus = exp(-in.t.^2/t2_Gauss^2);
% hold on
% plot(in.t,gaus)
% title('component Increasing exponential and Decreasing gausian')
% xlabel('Time (s)')
% ylabel('amp (a.u.)')

% combine the two funcitons to make the Lorentz-Gaussian conversion filter
filt = lor.*gaus;
figure
plot(in.t,filt)
title('Increasing exponential * Decreasing gausian')
xlabel('Time (s)')
ylabel('amp (a.u.)')
% 
% repeat filter to apply to eacha average
filtMtrx = repmat(filt.',[1 in.sz(2)]);

% apply filter
fids = fids.*filtMtrx;

%re-calculate Specs using fft
specs = fftshift(ifft(fids, [], in.dims.t),in.dims.t);

%FILLING IN DATA STRUCTURE
out=in;
out.fids=fids;
out.specs=specs;





