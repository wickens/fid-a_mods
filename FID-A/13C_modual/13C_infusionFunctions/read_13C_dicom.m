% This funtion reads in all the .dcm files in a folder 

function out = read_13C_dicom();
dicoms=findfiles_13C(pwd,'.dcm');

data = cell(numel(dicoms),1);
    for i=1:numel(dicoms)
        data{i,1}=io_loadspec_IMA_13C(dicoms{1,i},3,5000);
    end
 out = data; 
  