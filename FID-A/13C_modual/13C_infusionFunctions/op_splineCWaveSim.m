% Chris Wickens The University of Cambridge 060521:
%
% op_splineCWaveSim().m
%
% 
% USAGE:
% op_splineCWaveSim();
% 
% DESCRIPTION:
% Fits a multiple splines to CWAVE simulation (with no noise) so the curve can be solved at
% regular intervals 
% 
% INPUTS:
% CWaveSim          =   CWave simultion dynamic curve (contains time, conc and percentage enrichment information)
%
% 
%
% OUTPUTS:
% 

function [out] = op_splineCWaveSim(CWAVESim, pieces, timeRes)

% plot the profile before any processing 
figure;
plot(CWAVESim.time,CWAVESim.conc,'o')
title('CWave simulation before any processing')


% calculate the number of spline breaks
sz_x = length(CWAVESim.conc);
breaks = linspace(0,CWAVESim.time(end),pieces);

% fit the spline with the number of breaks specified
ppl = splinefit(CWAVESim.time, CWAVESim.conc, breaks);

% reconstruct time axis with the resolution I want to solve for 
time = 0:timeRes:CWAVESim.time(end);

splineSol = ppval(ppl,time);

plot(time, splineSol,'o')
title('Spline solution')

out.conc = splineSol.';
out.time = time.';

out.PE = CWAVESim.PE; % this is not used






















