% Chris W : 070220
% this function will take all the spectra in a time course as a cell
% It will the group the spectra you want to group together in the same
% FID-A structure ready for averaging

function [out] = AvTimeSeries(in, avFct)
    % in = should be the 13C time series (in cell format)
    % (in should have an even number of cells for averaging)
    % avfct = averaging factor (How many spectra you want to average per time point)
    
    len = size(in);
    len = len(1,1);
    
    Num_FIDSt = len/avFct;  % number of Fid a structures needed 
    
    % create a new cell where the new time series will be stored
    %Store all the odd structres in in_av_odd and even inside in_av_even 
    in_av_odd = in(1:2:end);
    in_av_even = in(2:2:end);
    
    % in_av_odd will be used as the base structue and averages from
    % in_av_even will be added to this 
    
    for i=1:Num_FIDSt
        in_av_odd{i,1}.fids(:,2)= in_av_even{i,1}.fids(:,1);
        
        % also reset the averaged flag to 0 in in_av_odd and increase
        % averages to avFct
        in_av_odd{i,1}.flags.averaged = 0;
        in_av_odd{i,1}.averages = avFct;
    end
    clear i
    
    in_av = in_av_odd;
    
    % average the structres in each cell 
    
    for ii=1:Num_FIDSt
        in_av{ii,1} = op_averaging(in_av{ii,1});
    end
    clear ii
    
    out = in_av;
    
    
    