% Chirs Wickens 130421: The University of Cambridge
%
% USAGE:
% out= op_splineDynamic(in, pieces)
% 
% DESCRIPTION:
% Perform cardinal b-spline fitting in the indirect domain of the dymanic time course.
% cardinal mean the knots (or break points are evenly spaced.
% 
% 
% INPUTS:
% in     = FIDA data where the whole infusion data set is stored in a single FID a structure.
% wndSz = windo size (number of averages to combine)
%
% OUTPUTS:
% out    = B-splined smothed dynamic time course.  


function [out] = op_splineDynamic(in, pieces)

% perform splining in the frequency domain 

specs = in.specs;
sz_x = in.sz(2);


breaks = linspace(1,sz_x,pieces);
pp1 = splinefit(1:1:sz_x,specs,breaks);

specSpline = ppval(pp1,1:1:sz_x);

figure
specNum = 1;%40;
plot(in.ppm,in.specs(:,specNum))
hold on
plot(in.ppm,specSpline(:,specNum))


%convert back to time domain
%if the length of Fids is odd, then you have to do a circshift of one to
%make sure that you don't introduce a small frequency shift into the fids
%vector.
if mod(size(specs,in.dims.t),2)==0
    %disp('Length of vector is even.  Doing normal conversion');
    fids=fft(fftshift(specSpline,in.dims.t),[],in.dims.t);
else
    %disp('Length of vector is odd.  Doing circshift by 1');
    fids=fft(circshift(fftshift(specSpline,in.dims.t),1),[],in.dims.t);
end


% fill output structure
out = in;
out.specs = specSpline;
out.pp1 = pp1;
out.fids = fids;





