% op_freqshift.m
% Chris Wickens, The University of Cambridge 2021.
% 
% USAGE:
% out=op_Average_block(in,group);
% 
% DESCRIPTION:
% Data where each average is stored in a seperate cell - averaged a
% specified block of data
% 
% INPUTS:
% in     = FIDA data where each aquisition is in its own cell.
% group = number of scans to group
%
% OUTPUTS:
% out    = cell structe conatingn black of averaged spectra.  


function out = op_Average_block(in, group)


in_reshp = reshape(in,group,[]); % reshape cell so that 

len = size(in_reshp);
len = len(2);
for i=1:len 
    New_avs{i,1} = op_comDataStruc(in_reshp(1:group,i));
    New_avs{i,1}.flags.averages = 0; % make sure the data set says it is unaveraged
    New_avs{i,1} = op_averaging(New_avs{i,1});
    
    New_avs{i,1}.dims.averages = 2; %Keep averages dimension 
end

out = New_avs;

