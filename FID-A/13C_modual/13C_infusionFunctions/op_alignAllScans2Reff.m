% op_alignAllScans2Reff.m
% Chris Wickens, The University of Cambridge 2020.
% 
% USAGE:
% out=op_alignAllScans2Reff(in,inRef);
% 
% DESCRIPTION:
% Takes average balse line and appends it to the start of in (infusion data) 
% then align scans to first BL averae bin using op_alignAllScans2Reff
% 
% INPUTS:
% in         = input data in matlab structure format.
% ppmmin     = minimum extent of frequency range in ppm.
% ppmmax     = maximum extent of frequency range in ppm.
%
% OUTPUTS:
% out        = Output following frequency range selection.



function [out] = op_alignAllScans2Reff(in,inRef)

% combine in and inRef
inAll = cat(1,inRef,in);

inAll_corr = op_alignAllScans(inAll,0.2046).';

% the remove the refference scan
inAll_corr = inAll_corr(2:end,1);

out = inAll_corr;


end
