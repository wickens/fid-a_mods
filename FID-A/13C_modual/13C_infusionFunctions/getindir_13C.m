%chris W

function out = getindir_13C(directory, match, r)
%fprintf(1,'Looking in %s for %s\n', directory, match)
d = dir(directory); 
out = []; 
n=0; 
for i=1:numel(d)
    if and(d(i).isdir, d(i).name(1)=='.'); continue; end; 
    
    if d(i).isdir
        add = getindir(fullfile(directory, d(i).name), match, r);
        out = [out add]; 
    else
        if ~r
            if strfind(d(i).name, match)
            %fprintf(1,'found\n');
            n=n+1;
            out{n} = fullfile(directory, d(i).name);
            end
        else
            if regexp(d(i).name, match) %strfind(d(i).name, match)
            %fprintf(1,'found\n');
            n=n+1;
            out{n} = fullfile(directory, d(i).name);
            end
        end
    end
end    