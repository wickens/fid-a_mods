% Chris W 13C : This function will read will look for a specified string
% in the out put to the funtion io_readSeqInfo.m

% output = a structure containing ProtocolName , Filename &
% SeriesDescription in that order

function [data_13C] = io_sortBySection_13C(fileStrs, seqInfo, calibChar_3, loadCorChar_4, baselineChar_5, INF60minChar_6, INFAltChar_6, LongTRnoNOEChar_7, LongTRnoNOE_char_phan_10)

%% data_13C.CalibScans_13C_3     
DICOMS_str = fileStrs.DICOMS_str;
numberOfdcm = length(DICOMS_str);
numberOfCalibs = 0;
for i=1:numberOfdcm
    if contains(seqInfo{i,1}.ProtocolName, calibChar_3) 
       numberOfCalibs = numberOfCalibs + 1;
    end
end

data_13C.CalibScans_13C_3 = strings([numberOfCalibs,3]);
calib_counter = 0;
for ii=1:numberOfdcm
    if contains(seqInfo{ii,1}.ProtocolName, calibChar_3) 
        calib_counter = calib_counter + 1;
        data_13C.CalibScans_13C_3(calib_counter,1) = seqInfo{ii,1}.ProtocolName;
        data_13C.CalibScans_13C_3(calib_counter,2) = seqInfo{ii,1}.Filename(end-19:end);
        data_13C.CalibScans_13C_3(calib_counter,3) = seqInfo{ii,1}.SeriesDescription;
    end
end

%% data_13C.Urea_loadCor_4 UREA ref singal    

numberOfUREA = 0;
for i=1:numberOfdcm
    if contains(seqInfo{i,1}.ProtocolName, loadCorChar_4) 
       numberOfUREA = numberOfUREA + 1;
    end
end

data_13C.Urea_loadCor_4 = strings([numberOfUREA,3]);
Urea_counter = 0;
for ii=1:numberOfdcm
    if contains(seqInfo{ii,1}.ProtocolName, loadCorChar_4) 
        Urea_counter = Urea_counter + 1;
        data_13C.Urea_loadCor_4(Urea_counter,1) = seqInfo{ii,1}.ProtocolName;
        data_13C.Urea_loadCor_4(Urea_counter,2) = seqInfo{ii,1}.Filename(end-19:end);
        data_13C.Urea_loadCor_4(Urea_counter,3) = seqInfo{ii,1}.SeriesDescription;
    end
end
clear i ii Urea_counter numberOfUREA


%% data_13C.Baseline_5 = struct([]);        

numberOfbaseline = 0;
for i=1:numberOfdcm
    if contains(seqInfo{i,1}.ProtocolName, baselineChar_5) 
       numberOfbaseline = numberOfbaseline + 1;
    end
end

data_13C.Baseline_5 = strings([numberOfbaseline,3]);
basline_counter = 0;
for ii=1:numberOfdcm
    if contains(seqInfo{ii,1}.ProtocolName, baselineChar_5) 
        basline_counter = basline_counter + 1;
        data_13C.Baseline_5(basline_counter,1) = seqInfo{ii,1}.ProtocolName;
        data_13C.Baseline_5(basline_counter,2) = seqInfo{ii,1}.Filename(end-19:end);
        data_13C.Baseline_5(basline_counter,3) = seqInfo{ii,1}.SeriesDescription;
    end
end
clear i ii basline_counter numberOfbaseline



%% data_13C.InfusionData_13C_6 = struct([]); 

numberOfInfus = 0;
for i=1:numberOfdcm
    if contains(seqInfo{i,1}.ProtocolName, INF60minChar_6) || contains(seqInfo{i,1}.ProtocolName, INFAltChar_6)
       numberOfInfus = numberOfInfus + 1;
    end
end

data_13C.InfusionData_13C_6 = strings([numberOfInfus,3]);
infus_counter = 0;
for ii=1:numberOfdcm
    if contains(seqInfo{ii,1}.ProtocolName, INF60minChar_6) || contains(seqInfo{ii,1}.ProtocolName, INFAltChar_6)
        infus_counter = infus_counter + 1;
        data_13C.InfusionData_13C_6(infus_counter,1) = seqInfo{ii,1}.ProtocolName;
        data_13C.InfusionData_13C_6(infus_counter,2) = seqInfo{ii,1}.Filename(end-19:end);
        data_13C.InfusionData_13C_6(infus_counter,3) = seqInfo{ii,1}.SeriesDescription;
    end
end

clear i ii infus_number infus_counter numberOfInfus


%% data_13C.LongTRnoNOE_7 = struct([]);      

numberOfNoNOE = 0;
for i=1:numberOfdcm
    if contains(seqInfo{i,1}.ProtocolName, LongTRnoNOEChar_7) 
       numberOfNoNOE = numberOfNoNOE + 1;
    end
end

data_13C.LongTRnoNOE_7 = strings([numberOfNoNOE,3]);
NoNOE_counter = 0;
for ii=1:numberOfdcm
    if contains(seqInfo{ii,1}.ProtocolName, LongTRnoNOEChar_7) 
        NoNOE_counter = NoNOE_counter + 1;
        data_13C.LongTRnoNOE_7(NoNOE_counter,1) = seqInfo{ii,1}.ProtocolName;
        data_13C.LongTRnoNOE_7(NoNOE_counter,2) = seqInfo{ii,1}.Filename(end-19:end);
        data_13C.LongTRnoNOE_7(NoNOE_counter,3) = seqInfo{ii,1}.SeriesDescription;
    end
end


%% data_13C.Phantom

numberOflongTR_Phan = 0;
for i=1:numberOfdcm
    if contains(seqInfo{i,1}.ProtocolName, LongTRnoNOE_char_phan_10) 
       numberOflongTR_Phan = numberOflongTR_Phan + 1;
    end
end

data_13C.LongTRnoNOE_10 = strings([numberOflongTR_Phan,3]);
Phantom_counter = 0;
for ii=1:numberOfdcm
    if contains(seqInfo{ii,1}.ProtocolName, LongTRnoNOE_char_phan_10) 
        Phantom_counter = Phantom_counter + 1;
        data_13C.LongTRnoNOE_10(Phantom_counter,1) = seqInfo{ii,1}.ProtocolName;
        data_13C.LongTRnoNOE_10(Phantom_counter,2) = seqInfo{ii,1}.Filename(end-19:end);
        data_13C.LongTRnoNOE_10(Phantom_counter,3) = seqInfo{ii,1}.SeriesDescription;
    end
end

%clear i ii NoNOE_counter numberOfNoNOE numberOfdcm

% comment out when debugging etc
%clear dir DICOMS dirName fileNames fids name_char info
