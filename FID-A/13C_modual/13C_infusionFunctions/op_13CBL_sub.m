% op_13CBL_sub.m
% Chris Wickens, The University of Cambridge 2020.
% 
% USAGE:
% out=op_13CBL_sub(in,BL,type);
% 
% DESCRIPTION:
% Take in 13C infusion data with correspoonding baseline data and subtract
% baeline signal from each infusion time point 
% 
% INPUTS:
% in     = infusion data stored in cell array
% BL     = Baseline data
% type   = if baseline data has not been average type = 1, 
%          if it has been averaged then type = 2
%
% OUTPUTS:
% out        = Output following frequency range selection.


function out=op_13CBL_sub(in,BL,type);

inf_d = in;
clear in

if strcmp(type,'1') % averages the whole baseline
    group = numel(BL); % how many averages are in the baseline
    BL = op_Average_block(BL,group);
    
end

% subtract BL data from the infusion data 
for i=1:numel(inf_d)
    inf_d_sub{i,1} = op_subtractScans(inf_d{i,1},BL{1});
end

out = inf_d_sub;



