% Chris W : this function will take in a cell containing 
          % multiple fid a files and make JMRUI .txt files for all of them
          
          % This function out puts JMRUI files .txt without the need to out put a matlab variable   
    
          
          %% Write JMRUI file for fitting infusion minus Baseline 
          function io_multi_writejmrui(in) 

          sz = size(in);
          sz = sz(1,1);

            for i=1:sz
                filename = in{i,1}.filename(1:end-3);
                filename = strcat(filename, 'txt');
                RF=io_writejmrui(in{i,1},filename);
            end 
            
                    clear i sz filename RF
          end