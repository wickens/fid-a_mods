% Chris Wickens, The University of Cambridge 2021.    
% 
% op_convtFIDA2OXSA.m
%
% USAGE:
% [out] = op_convtFIDA2OXSA(in, offset)
% 
% DESCRIPTION:
% This function converts FIDA data stucture into OXSA data structure to be
% fitted by OXSA AMARES fitting routines 
% 
% INPUTS:
% in         = Data in fid A data structure
% offset     = offset of data 
% 
%
% OUTPUTS:
% inputFid     = FID data to be fitted  
% exptParams   = data in a file structure that oxsa can read and fit
% 

function [inputFid, exptParams] = op_convtFIDA2OXSA(in, offset)


% extract fid vector for fitting 
inputFid = in.fids;

% build and fill OXSA fields 
% required by AMARES
exptParams.samples = in.sz(1);  % vector size 
exptParams.imagingFrequency = in.Bo*10.7084; %/ MHz   -- siemens 3T scanners are not actually 3T they are just below for my 13C I have assumed they are 3T. I should correct this aas it sults in slight ppm refference errors 
%exptParams.imagingFrequency = 2.89362*10.7084; % Chris W : temp change 280321
exptParams.timeAxis = in.t.';   %/ s
exptParams.dwellTime = in.dwelltime;    %/ s
exptParams.ppmAxis = fliplr(in.ppm).';     %/ ppm
exptParams.beginTime = 0;  %/ s   -- I am will just put this as zero like in the OXSA example -- is this correct 

% additional ones for completeness 
exptParams.BW = in.spectralwidth;  % hz
exptParams.offset = offset; %   e.g. -153.00;









