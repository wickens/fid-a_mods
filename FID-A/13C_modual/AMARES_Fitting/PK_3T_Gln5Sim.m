% Chris Wickens, The University of Cambridge 2021.    
% 
% PK_3T_Gln5Sim.m
%
% USAGE:
% [out] = PK_3T_Gln5Sim()
% 
% DESCRIPTION:
% creates .M file to assemble the bounds, priorKnowledge and initialValues 
% structs for the matlab implementation of AMARES
%
% Each of B, PK and IV is a 1xN struct, where N is the number of peaks. Note
% multiplets are counted as one peak.
% The fields are as follows:
% bounds           initialValues          priorKnowledge

% peakName         peakName               peakName
% chemShift        chemShift              multiplet
% linewidth        linewidth              chemShiftDelta
% amplitude        amplitude              amplitudeRatio
% phase            phase                  G_linewidth
% chemShiftDelta                          G_amplitude
% amplitudeRatio                          G_phase
%                                        G_chemShiftDelta
%                                        refPeak



% INPUTS:
% 
%
% OUTPUTS:
% out     = prior knowled for a gln5 13C MRS simulation   
% 
% 

function [outStruct] = PK_3T_Gln5Sim()
%% Bounds
fields.Bounds = {
'peakName',                                 'chemShift',     'linewidth',   'amplitude',    'phase',     'chemShiftDelta',   'amplitudeRatio'};
values.boundsCellArray = {...
'GlnC5',                                     [178.3,179.5],       [1,4],       [0,inf],     [0,360],                [],                [];                            
};

%% initialValues
fields.IV = {
'peakName',                                   'chemShift',     'linewidth',   'amplitude',    'phase'};
values.IVCellArray = {...
'GlnC5',                                            178.5,           3,         1,               0;
};


fields.PK = {
'peakName',                                 'multiplet',     'chemShiftDelta',   'amplitudeRatio',    'G_linewidth',   'G_amplitude',    'G_phase'   ,'RelPhase',  'G_chemShiftDelta',   'refPeak'}; % refPeak= highest peak in the spectrum 
values.PKCellArray = {...
'GlnC5',                                       [],                 [],                  [],                 [],             [],               1,          [],            [],                  1;

};


%% 
fields.PK = {
'peakName',                                 'multiplet',     'chemShiftDelta',   'amplitudeRatio',    'G_linewidth',   'G_amplitude',    'G_phase'   ,'RelPhase',  'G_chemShiftDelta',   'refPeak'};
values.PKCellArray = {...
'GlnC5',                                           [],                [],                  [],                  [],            [],                1,        [],          [],                 1;
};

%% Pass to the function which assembles the constraints into structs and saves them
outStruct = AMARES.priorKnowledge.preparePriorKnowledge(fields,values);
outStruct.svnVersion = '$Rev: 7662 $';
outStruct.svnHeader = '$Header: https://cardiosvn.fmrib.ox.ac.uk/repos/crodgers/FromJalapeno/MATLAB/RodgersSpectroToolsV2/main/+AMARES/+priorKnowledge/PK_7T_Cardiac.m 7662 2021-03-26 13:26:32Z chris Wickens $';
