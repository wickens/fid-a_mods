% Chris Wickens, The University of Cambridge 2021.    
% 
% PK_3T_13CGlcC2_InViv.m
%
% USAGE:
% [out] = PK_3T_13CGlcC2_InVivo_Voigt()
% 
% DESCRIPTION:
% creates .M file to assemble the bounds, priorKnowledge and initialValues 
% structs for the matlab implementation of AMARES. This is prior knoledge 
% for [2-13C]glc infusion. Down stream metabolites smiulated are GlcC2 (alpha/beta)
% GluC5, GlnC5 and AspC4 are fitted. This is for invivo data. --- I think
% this include mixed Lorenzian gaussian fitting (Voigt line shapes)
%
% Each of B, PK and IV is a 1xN struct, where N is the number of peaks. Note
% multiplets are counted as one peak.
% The fields are as follows:
% bounds           initialValues          priorKnowledge

% peakName         peakName               peakName
% chemShift        chemShift              multiplet
% linewidth        linewidth              chemShiftDelta
% amplitude        amplitude              amplitudeRatio
% phase            phase                  G_linewidth
% chemShiftDelta                          G_amplitude
% amplitudeRatio                          G_phase
%                                        G_chemShiftDelta
%                                        refPeak



% INPUTS:
% 
%
% OUTPUTS:
% out     = prior knowled for a Glc5 infusion time course 13C MRS simulation   
%           Down stream metabolites smiulated are GlcC2 (alpha/beta)
%           GluC5, GlnC5 and AspC4 are fitted.
% 
% 

function [outStruct] = PK_3T_13CGlcC2_InVivo_Voigt()
%% Bounds
fields.Bounds = {
'peakName',                                 'chemShift',     'linewidth',   'amplitude',    'phase',     'chemShiftDelta',   'amplitudeRatio', 'sigma'};
values.boundsCellArray = {...
'GlcC2Alpha',                                [71.0,73.7],       [1,15],       [0,inf],     [-5,5],                [],                [],      [0 0.01];                            
'GlcC2Beta',                                 [74.0,76.5],       [1,15],       [0,inf],     [-20,20],                [],                [],     [0 0.01];                            
'GluC5',                                     [181,185.5],       [1,8],       [0,inf],     [-10,10],                [],                [],      [0 0.01];                            
'GlnC5',                                     [178.3,179.5],       [2.5,5],       [0,inf],     [-10,10],                [],                [],     [0 inf];                           
'AspC4',                                     [177.5, 178.3],      [2.5,5],        [0,inf],    [-10,10],                [],                [],      [0 inf] ;                            
};

%% initialValues
fields.IV = {
'peakName',                                   'chemShift',     'linewidth',   'amplitude',    'phase', 'sigma'};
values.IVCellArray = {...
'GlcC2Alpha',                                       72.5,           8,         1,               0,    0;
'GlcC2Beta',                                        75.2,           8,         1,               0,    0;
'GluC5',                                            182.0,           3,         1,               0,    0;
'GlnC5',                                            178.5,           3,         1,               0,    5;
'AspC4',                                            178.25,          3,          1,              0,    5;
};


fields.PK = {
'peakName',                                 'multiplet',     'chemShiftDelta',   'amplitudeRatio',    'G_linewidth',   'G_amplitude',    'G_phase'   ,'RelPhase',  'G_chemShiftDelta',   'refPeak'}; % refPeak= highest peak in the spectrum 
values.PKCellArray = {...
'GlcC2Alpha',                                  [],                 [],                  [],                 [],             [],               1,          [],            [],                  0;
'GlcC2Beta',                                   [],                 [],                  [],                 [],             [],               1,          [],            [],                  1;
'GluC5',                                       [],                 [],                  [],                 [],             [],               1,          [],            [],                  0;
'GlnC5',                                       [],                 [],                  [],                 [],             [],               1,          [],            [],                  0;
'AspC4',                                       [],                 [],                  [],                 [],             [],               1,          [],            [],                  0;

};


%% 
fields.PK = {
'peakName',                                 'multiplet',     'chemShiftDelta',   'amplitudeRatio',    'G_linewidth',   'G_amplitude',    'G_phase'   ,'RelPhase',  'G_chemShiftDelta',   'refPeak'};
values.PKCellArray = {...
'GlcC2Alpha',                                      [],                [],                  [],                  [],            [],                1,        [],          [],                 0;
'GlcC2Beta',                                       [],                [],                  [],                  [],            [],                1,        [],          [],                 1;
'GluC5',                                           [],                [],                  [],                  [],            [],                1,        [],          [],                 0;
'GlnC5',                                           [],                [],                  [],                  [],            [],                1,        [],          [],                 0;
'AspC4',                                           [],                [],                  [],                  [],            [],                1,        [],          [],                 0;
};

%% Pass to the function which assembles the constraints into structs and saves them
outStruct = AMARES.priorKnowledge.preparePriorKnowledge(fields,values);
outStruct.svnVersion = '$Rev: 7662 $';
outStruct.svnHeader = '$Header: https://cardiosvn.fmrib.ox.ac.uk/repos/crodgers/FromJalapeno/MATLAB/RodgersSpectroToolsV2/main/+AMARES/+priorKnowledge/PK_7T_Cardiac.m 7662 2021-03-26 13:26:32Z chris Wickens $';
