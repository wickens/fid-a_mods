% Chris W (280321): here I want to work why fitting my FIDA data in OXSA AMARES environment
% is not working ... 


%% This is the closest to a working exampl I have 
load FIDAstructReff.mat
[inputFid, exptParams] = op_convtFIDA2OXSA(op_addNoise(FIDAstruct,1));
pk = PK_3T_Glu5Sim();
varargin = struct('firstOrder',0);
[fitResults fitStatus figureHandle CRBResults] = AMARES.amaresFit(inputFid(:,1), exptParams, pk, 100,varargin);


%% 

load FIDAstructReff.mat
[inputFid, exptParams] = op_convtFIDA2OXSA(op_addNoise(FIDAstruct,1),-153);
pk = PK_3T_Glu5Sim_test();
varargin = struct('firstOrder',0);
[fitResults fitStatus figureHandle CRBResults] = AMARES.amaresFit(inputFid(:,1), exptParams, pk, 100,varargin);


%% do analysis on-reffercenced Glu C5 peak 
load FIDAstruct.mat
%[inputFid, exptParams] = op_convtFIDA2OXSA(op_addNoise(FIDAstruct,0.1),-153);
[inputFid, exptParams] = op_convtFIDA2OXSA(FIDAstruct,-153);
pk = PK_3T_Glu5Sim_test();
varargin = struct('firstOrder',0);
[fitResults fitStatus figureHandle CRBResults] = AMARES.amaresFit(inputFid(:,1), exptParams, pk, 100,varargin);


disp(fitResults.amplitude)


%% do analysis on-reffercenced Glu C5 peak -- with correct vaues for prior info
load FIDAstruct.mat
%[inputFid, exptParams] = op_convtFIDA2OXSA(op_addNoise(FIDAstruct,1.5),-153);
[inputFid, exptParams] = op_convtFIDA2OXSA(FIDAstruct,-153);

pk = PK_3T_Glu5Sim();
% apply the offset
pk.initialValues.chemShift = (pk.initialValues.chemShift + exptParams.offset);
pk.bounds.chemShift = (pk.bounds.chemShift + exptParams.offset);
varargin = struct('firstOrder',0);
[fitResults fitStatus figureHandle CRBResults] = AMARES.amaresFit(inputFid(:,1), exptParams, pk, 100,varargin);


disp(fitResults.amplitude)





