% Chris Wickens, The University of Cambridge 2021.    
% 
% fit_infData.m
%
% USAGE:
% [fitResults fitStatus figureHandle CRBResults] = fit_infData(in, pk, plot)
% 
% DESCRIPTION:
% This function fits infusion data by OXSA AMARES fitting routines. Results for 
% each spectrum are strored in there own cell structure element 
% 
% INPUTS:
% in         = Data in fid A data structure
% pk     = Prior knowledge for fitting 
% plot    = figure number of output plot (-- if false output plot supressed)
%
% OUTPUTS
% NB(for infusion data each spectra has these feild in there own cell structre)
%
% fitResults =  is a structure containing the fitted parameters.
%
% fitStatus =  is a structure containing other information regarding the
% best-fit solution.
%
% figureHandle = contains the handle of the fit results plot (or [] if
% fitting was run without plotting the results).
%
% CRBResults =  is a structure containing the error estimates of the fitted
% parameters.
%
% amp_ArrFileds = is an array of all the amplitudes in the infusion time course 
%                 Where each field corresponds to a different metabolite
%



function [Results, amp_ArrFileds] = fit_infData(in, pk, plotOn, offset)
    
    % create feilds to store infusion time course results in 
    Results = cell(numel(in),1);
    

    % fit each spectra in the infusion time course
    for i = 1:numel(in)
        
        if ~isa(in{i,1}.fids,'double') % if data is not double presiion array then conver it
            in{i,1}.fids = double(in{i,1}.fids);
            in{i,1}.specs = double(in{i,1}.specs);
            
            in{i,1}=op_complexConj_13C(in{i,1});
        end
        
        
        
        % convert data into OXSA data structe
        [inputFid, exptParams] = op_convtFIDA2OXSA(in{i,1},offset); % for glu sim offset = -153

        % apply the offset
        %pk = PK_3T_Glu5Sim();
        pk_rep = pk;
        
        % apply the offset
        for ii=1:length(pk_rep.initialValues)
            pk_rep.initialValues(ii).chemShift = (pk_rep.initialValues(ii).chemShift + exptParams.offset);
            pk_rep.bounds(ii).chemShift = (pk_rep.bounds(ii).chemShift + exptParams.offset);
        end
        clear ii
        varargin = struct('firstOrder',0);
        
        [Results{i,1}.fitResults, Results{i,1}.fitStatus, Results{i,1}.figureHandle, ...
            Results{i,1}.CRBResults] = AMARES.amaresFit(inputFid(:,1), ...
            exptParams, pk_rep, plotOn,varargin);
        
        clear inputFid exptParams pk_rep
    end 
    
    % axtract amp results into the same array
    %amp_Arr = struct(numel(Results{i},1));
    for idx=1:numel(Results)
        amp_Arr(idx,:) = Results{idx,1}.fitResults.amplitude;            
    end
    
    
    %%
    if ~iscell([pk.initialValues.peakName]) % if there are no cells in pk.initialValues.peakName -- put the field names in a cell
        l_metab = {pk.initialValues.peakName};
    else
        l_metab = [pk.initialValues.peakName];
    end
    
    for i=1:length(l_metab)
        amp_ArrFileds.(l_metab{i}) = amp_Arr(:,i); 
    end
    clear i
    
    %% store data in stuture of multiple fields 
%     if length(pk.initialValues) == 1
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1));
%     elseif length(pk.initialValues) == 2
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2));
%     elseif length(pk.initialValues) == 3
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2), ...
%             pk.initialValues(3).peakName, amp_Arr(:,3));
%     elseif length(pk.initialValues) == 4
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2), ...
%             pk.initialValues(3).peakName, amp_Arr(:,3), ...
%             pk.initialValues(4).peakName, amp_Arr(:,4));        
%     elseif length(pk.initialValues) == 5
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2), ...
%             pk.initialValues(3).peakName, amp_Arr(:,3), ...
%             pk.initialValues(4).peakName, amp_Arr(:,4), ...
%             pk.initialValues(5).peakName, amp_Arr(:,5));        
%     elseif length(pk.initialValues) == 6
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2), ...
%             pk.initialValues(3).peakName, amp_Arr(:,3), ...
%             pk.initialValues(4).peakName, amp_Arr(:,4), ...
%             pk.initialValues(5).peakName, amp_Arr(:,5), ...
%             pk.initialValues(6).peakName, amp_Arr(:,6));        
%     elseif length(pk.initialValues) == 7
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2), ...
%             pk.initialValues(3).peakName, amp_Arr(:,3), ...
%             pk.initialValues(4).peakName, amp_Arr(:,4), ...
%             pk.initialValues(5).peakName, amp_Arr(:,5), ...
%             pk.initialValues(6).peakName, amp_Arr(:,6), ...
%             pk.initialValues(7).peakName, amp_Arr(:,7));        
%     elseif length(pk.initialValues) == 8
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2), ...
%             pk.initialValues(3).peakName, amp_Arr(:,3), ...
%             pk.initialValues(4).peakName, amp_Arr(:,4), ...
%             pk.initialValues(5).peakName, amp_Arr(:,5), ...
%             pk.initialValues(6).peakName, amp_Arr(:,6), ...
%             pk.initialValues(7).peakName, amp_Arr(:,7), ...
%             pk.initialValues(8).peakName, amp_Arr(:,8));        
%     elseif length(pk.initialValues) == 9
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2), ...
%             pk.initialValues(3).peakName, amp_Arr(:,3), ...
%             pk.initialValues(4).peakName, amp_Arr(:,4), ...
%             pk.initialValues(5).peakName, amp_Arr(:,5), ...
%             pk.initialValues(6).peakName, amp_Arr(:,6), ...
%             pk.initialValues(7).peakName, amp_Arr(:,7), ...
%             pk.initialValues(8).peakName, amp_Arr(:,8), ...
%             pk.initialValues(9).peakName, amp_Arr(:,9));                
%     elseif length(pk.initialValues) == 10
%         amp_ArrFileds = struct(pk.initialValues(1).peakName, amp_Arr(:,1), ...
%             pk.initialValues(2).peakName, amp_Arr(:,2), ...
%             pk.initialValues(3).peakName, amp_Arr(:,3), ...
%             pk.initialValues(4).peakName, amp_Arr(:,4), ...
%             pk.initialValues(5).peakName, amp_Arr(:,5), ...
%             pk.initialValues(6).peakName, amp_Arr(:,6), ...
%             pk.initialValues(7).peakName, amp_Arr(:,7), ... 
%             pk.initialValues(8).peakName, amp_Arr(:,8), ...
%             pk.initialValues(9).peakName, amp_Arr(:,9), ... 
%             pk.initialValues(10).peakName, amp_Arr(:,10));        
%     end
end 





