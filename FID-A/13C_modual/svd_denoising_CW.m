function [out, ax_U, ax_V, ax_singVal] = svd_denoising_CW( data, Rank )
% I have modified this code from Liam Youngs code svd_denoising_LY 
% Perform SVD de-noising of time-series data
%
% Input: 
% data   : Spectra in FID-A data structure. Note all timeseries data must
%          be in 1 array 
% Rank :   What rank should my truncated vector be 
%
% output:
% out   = denoised data in fid A file format
% ax_U  = figure handle of U single vecto plot
% ax_V  = figure handle of V single vecto plot
%
%

%Rank = 10; % Hard coded for 31P MRS spectra
threshold = 1e-5; % Arbitrarily chosen. Should leave in very small signal contributions... probably at the cost of some noise

% this bit is for doing the SVD operation in the frequency domain 
% [U,S,V] = svd(data.specs);
% 
% Snew = S;
% Snoise = S;
% 
% if method == 1
%     Snew((Rank+1):end, (Rank+1):end) = 0;
%     Snoise(1:Rank, 1:Rank) = 0;
% elseif method == 2
%     Snew( Snew < threshold ) = 0;
%     Snoise(Snoise < threshold ) = 0;
% end
% 
% specsDenoised = U*Snew*V';
% noise = U*Snoise*V';
    
% % Note that this is a linear operation so can be done in either frequency
% % or time-domain with exactly the same results... 
[U_fid,S_fid,V_fid] = svd(data.fids);

% The 'noise' singular values that I throw away when reconstructing my spectra 
Snoise = S_fid;
Snoise(1:Rank, 1:Rank) = 0;
noise = Snoise; 

Snew = S_fid; 
Snew((Rank+1):end, (Rank+1):end) = 0; 
 
fidsDenoised = U_fid*Snew*V_fid';
 
specsDenoised = fftshift(ifft(fidsDenoised,[],data.dims.t),data.dims.t);

% make some plots 
% make left (U) matrix plots 
% figure
% for i=1:4
%     subplot(4,1,i)
%     ppmreg = data.ppm(data.ppm > 20 & data.ppm<40);
%     sig = fftshift(ifft(U_fid(:,i),[],data.dims.t),data.dims.t);
%     sigReg = sig(data.ppm > 20 & data.ppm<40);
%     plot(ppmreg,sigReg);
%     %plot(ppmreg,fftshift(ifft(U_fid(:,i),[],data.dims.t),data.dims.t));
%     colnumstr = num2str(i);
%     title(['U colum number ' colnumstr]);
%     xlabel('PPM')
%     ylabel('amp (a.u)')
%     ylim([-0.01 0.01]);
%     
% end
% clear i colnumstr
% ax_U = gcf;
% set(gcf, 'Position',  [500, 100, 400, 700])
% 
% % make left (V) matrix plots 
% sz_v = size(V_fid); % get size of V matrix
% sz_v(1)=sz_v(1);
% figure
% for i=1:8
%     subplot(4,2,i);
%     plot(1:1:sz_v,V_fid(:,i))
%     colnumstr = num2str(i);
%     title(['V colum number ' colnumstr]);
%     xlabel('Specturm number')
%     ylabel('amp (a.u)')
%     ylim([-0.1 0.1]);
% end
% clear i colnumstr
% set(gcf, 'Position',  [1000, 100, 500, 700])
% 
% ax_V = gcf;
% 
% % plot singular valuse 
% figure
% semilogy(1:1:length(diag(S_fid)), diag(S_fid), 'LineWidth',2.0)
% title('Singular values')
% xlabel('Singular value')
% ylabel('log(Varience) (a.u.)')
% xlim([-15 600])
% ax_singVal = gcf;

out = data; 
out.specs = specsDenoised; 
out.noise = noise;
% chris W:
out.fids = fidsDenoised;



